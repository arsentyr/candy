#ifndef CORE_BASS_H
#define CORE_BASS_H

#include <BASS/bass.h>

#ifdef NDEBUG
#	define CHECK_BASS() ((void)0)
#else
#	include "Assert.h"
#	define CHECK_BASS() do { auto error = BASS_ErrorGetCode(); ASSERTF(error == BASS_OK, "BASS error %d.", error); } while (false)
#endif /* NDEBUG */

#endif /* CORE_BASS_H */
