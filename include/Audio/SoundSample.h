#ifndef CORE_SOUND_SAMPLE_H
#define CORE_SOUND_SAMPLE_H

#include "Audio/BASS.h"

namespace Core {

/* SoundSample */

class SoundSample final {
public:
	SoundSample() noexcept = default;
	SoundSample(const SoundSample&) = delete;
	SoundSample(SoundSample&&) = default;

	SoundSample& operator=(const SoundSample&) = delete;
	SoundSample& operator=(SoundSample&&) = delete;

	~SoundSample();

	void Load(const void* data, int length);

	void Unload();

	bool IsLoaded() const;

	void Play(float volume = 1);

	void Stop();

private:
	static constexpr HSAMPLE UNKNOWN_SAMPLE = 0;

	HSAMPLE _sample = UNKNOWN_SAMPLE;
};

} /* namespace Core */

#endif /* CORE_SOUND_SAMPLE_H */
