#ifndef CORE_SOUND_STREAM_H
#define CORE_SOUND_STREAM_H

#include "Audio/BASS.h"
#include "Content/DataReader.h"

namespace Core {

/* SoundStream */

class SoundStream final {
public:
	SoundStream() noexcept = default;
	SoundStream(const SoundStream&) = delete;
	SoundStream(SoundStream&&) = default;

	SoundStream& operator=(const SoundStream&) = delete;
	SoundStream& operator=(SoundStream&&) = delete;

	~SoundStream();

	void Load(DataReader& reader);

	void Unload();

	bool IsLoaded() const;

	void Play(float volume = 1);

private:
	static constexpr HSTREAM UNKNOWN_STREAM = 0;

	HSTREAM _stream = UNKNOWN_STREAM;
};

} /* namespace Core */

#endif /* CORE_SOUND_STREAM_H */
