#ifndef CORE_SOUND_H
#define CORE_SOUND_H

namespace Core {

/* Sound */

class Sound final {
public:
	Sound() noexcept = default;
	Sound(const Sound&) = delete;
	Sound(Sound&&) = delete;

	Sound& operator=(const Sound&) = delete;
	Sound& operator=(Sound&&) = delete;
	
	~Sound();

	void Initialize();

	void Resume();

	void Pause();

	void Release();
};

extern Sound& Sfx;

} /* namespace Core */

#endif /* CORE_SOUND_H */
