#ifndef CANDY_CHIP_H
#define CANDY_CHIP_H

#include "Core.h"

namespace Candy {

enum class ChipType {
	Hidden,
	Cake,
	Candy,
	Donut,
	Star
};

/* Chip */

class Chip {
public:
	static constexpr int Width() { return 73; }

	static constexpr int Height() { return 73; }

public:
	explicit Chip(bool isLocked);

	virtual ~Chip() = 0;

	virtual void SetPosition(float x, float y);

	void SetPosition(const Vector2& position);

	virtual float Left() const;

	virtual float Right() const;

	virtual float Bottom() const;

	virtual float Top() const;

	virtual ChipType Type() const = 0;

	void Lock();

	void Unlock();

	bool IsLocked() const;

	virtual void Update(float /*deltaTime*/) {}

	virtual void Draw() const = 0;

private:
	float _x = 0;
	float _y = 0;
	bool _isLocked;
};

/* Hidden */

class Hidden : public Chip {
public:
	Hidden();

	void Draw() const override;

	ChipType Type() const override;
};

/* Cake */

class Cake : public Chip {
public:
	Cake();

	void SetPosition(float x, float y) override;

	void Draw() const override;

	ChipType Type() const override;

private:
	Sprite _cake;
};

/* Candy */

class Candy : public Chip {
public:
	Candy();

	void SetPosition(float x, float y) override;

	void Draw() const override;

	ChipType Type() const override;

private:
	Sprite _candy;
};

/* Donut */

class Donut : public Chip {
public:
	Donut();

	void SetPosition(float x, float y) override;

	void Draw() const override;

	ChipType Type() const override;

private:
	Sprite _donut;
};

/* Star */

class Star : public Chip {
public:
	Star();

	void SetPosition(float x, float y) override;

	void Update(float deltaTime) override;

	void Draw() const override;

	ChipType Type() const override;

private:
	Animation _star;
	Tweenf _dy;
	Tweenf _alpha;
};

} /* namespace Candy */

#endif /* CANDY_CHIP_H */
