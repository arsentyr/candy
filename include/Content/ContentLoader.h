#ifndef CORE_CONTENT_LOADER_H
#define CORE_CONTENT_LOADER_H

#include <array>
#include <string>
#include "Audio/SoundSample.h"
#include "Audio/SoundStream.h"
#include "BuildConfig.h"
#include "Graphics/Texture.h"

namespace Core {

/* ContentLoader */

class ContentLoader {
public:
	ContentLoader() = default;
	ContentLoader(const ContentLoader&) = delete;
	ContentLoader(ContentLoader&&) = delete;

	ContentLoader& operator=(const ContentLoader&) = delete;
	ContentLoader& operator=(ContentLoader&&) = delete;

	virtual ~ContentLoader() = default;

	virtual void LoadTexture(Texture& texture, const std::string& path) = 0;

	virtual void LoadSoundSample(SoundSample& sample, const std::string& path) = 0;

	virtual void LoadSoundStream(SoundStream& stream, const std::string& path) = 0;

protected:
	static void* Buffer() {
		static std::array<char, BuildConfig::LOADER_BUFFER_SIZE> buffer;
		return buffer.data();
	}
};

extern ContentLoader& Loader;

} /* namespace Core */

#endif /* CORE_CONTENT_LOADER_H */
