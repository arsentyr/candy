#ifndef CORE_DATA_READER_H
#define CORE_DATA_READER_H

namespace Core {

/* DataReader */

class DataReader {
public:
	DataReader() = default;
	DataReader(const DataReader&) = delete;
	DataReader(DataReader&&) = delete;

	DataReader& operator=(const DataReader&) = delete;
	DataReader& operator=(DataReader&&) = delete;

	virtual ~DataReader() = default;

	virtual const char* Name() const = 0;

	virtual bool Open(const char* name) = 0;

	virtual bool IsOpen() const = 0;

	virtual long int Read(void* buffer, unsigned int count) = 0;

	virtual long int Seek(long int offset, int whence) = 0;

	virtual long int Tell() = 0;

	virtual long int Length() const = 0;

	virtual void Close() = 0;
};

} /* namespace Core */

#endif /* CORE_DATA_READER_H */
