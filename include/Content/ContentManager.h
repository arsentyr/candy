#ifndef CORE_CONTENT_MANAGER_H
#define CORE_CONTENT_MANAGER_H

#include <string>
#include <unordered_map>
#include "Animation/Animation.h"
#include "Audio/SoundSample.h"
#include "Audio/SoundStream.h"
#include "Graphics/Texture.h"

namespace Core {

/* ContentManager */

class ContentManager final {
public:
	ContentManager();
	ContentManager(const ContentManager&) = delete;
	ContentManager(ContentManager&&) = delete;

	ContentManager& operator=(const ContentManager&) = delete;
	ContentManager& operator=(ContentManager&&) = delete;

	~ContentManager() = default;

	void Initialize();

	void Reload();

	void Release();

	Texture& GetTexture(const std::string& textureId);

	AnimationContent& GetAnimation(const std::string& animationId);

	SoundSample& GetSoundSample(const std::string& sampleId);

	SoundStream& GetSoundStream();

private:
	void CreateTextures();

	void CreateAnimations();

	void CreateProgressAnimation();

	void CreateStarAnimation();

	void CreateStarDisappearAnimation();

	void CreateOmNomFirstAnimation();

	void CreateOmNomSecondAnimation();

	void CreateConfettiAnimation();

	void CreateSounds();

private:
	std::unordered_map<std::string, Texture> _textures;
	std::unordered_map<std::string, AnimationContent> _animations;
	std::unordered_map<std::string, SoundSample> _samples;
	SoundStream _stream;
};

extern ContentManager& Content;

} /* namespace Core */

#endif /* CORE_CONTENT_MANAGER_H */
