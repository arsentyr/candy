#ifndef CORE_IMAGE_H
#define CORE_IMAGE_H

#include "Content/DataReader.h"

namespace Core {

void DecodePng(DataReader& reader, void* buffer, unsigned int* width, unsigned int* height);

} /* namespace Core */

#endif /* CORE_IMAGE_H */
