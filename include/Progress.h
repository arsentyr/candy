#ifndef CANDY_PROGRESS_H
#define CANDY_PROGRESS_H

#include "Core.h"
#include "View.h"

namespace Candy {

enum class ProgressState {
	None,
	Low,
	Medium,
	High
};

/* Progress */

class Progress : public View {
public:
	void SetPosition(float x, float y);

	void SetState(ProgressState state);

	void Increase();

	int Width() const;

	int Height() const;

public:	/* View Implementaion */
	void Update(float deltaTime) override;

	void Draw() override;

private:
	ProgressState _state = ProgressState::None;
	Animation _left = Animation("progress");
	Animation _middle = Animation("progress");
	Animation _right = Animation("progress");
};

} /* namespace Candy */

#endif /* CANDY_PROGRESS_H */
