#ifndef CANDY_EFFECT_H
#define CANDY_EFFECT_H

#include <list>
#include "Cell.h"
#include "Core.h"

namespace Candy {

/* Effect */

class Effect {
public:
	virtual ~Effect() = default;

	virtual void Update(float deltaTime) = 0;

	virtual void Draw() = 0;

	virtual bool IsFinished() const = 0;
};

/* SwapEffect */

class SwapEffect : public Effect {
public:
	SwapEffect(Chip& first, Chip& second, float duration);

	virtual ~SwapEffect();

	void Update(float deltaTime) override;

	void Draw() override;

	bool IsFinished() const override;

private:
	void Finish();

private:
	Chip& _first;
	Chip& _second;
	Tweenv _firstPosition;
	Tweenv _secondPosition;
	std::list<Vector2> _secondTrail;
};

/* PingPongEffect */

class PingPongEffect : public Effect {
public:
	PingPongEffect(Chip& first, Chip& second, float duration);

	virtual ~PingPongEffect();

	void Update(float deltaTime) override;

	void Draw() override;

	bool IsFinished() const override;

private:
	void Finish();

private:
	Chip& _first;
	Chip& _second;
	Tweenv _firstPosition;
	Tweenv _secondPosition;
	bool _isForward = true;
};

/* DropEffect */

class DropEffect : public Effect {
public:
	DropEffect(Chip& chip, float top, float bottom, float duration, float alpha = 1, float delay = 0);

	virtual ~DropEffect();

	void Update(float deltaTime) override;

	void Draw() override;

	bool IsFinished() const override;

private:
	void Finish();

private:
	Chip& _chip;
	Tweenf _bottom;
	Tweenf _alpha;
	float _delay;
};

/* RemoveEffect */

class RemoveEffect : public Effect {
public:
	RemoveEffect(Chip& chip, float x0, float y0, float x1, float y1, float duration);

	virtual ~RemoveEffect();

	void Update(float deltaTime) override;

	void Draw() override;

	bool IsFinished() const override;

private:
	void Finish();

private:
	Chip& _chip;
	Tweenf _x = Tweenf(0, 0, 1, TweenEasing::QuadOut, TweenMode::Forward, true);
	Tweenf _y = Tweenf(0, 0, 1, TweenEasing::QuadIn, TweenMode::Forward, true);
	Tweenf _alpha = Tweenf(1, 0.6, 1, TweenEasing::QuadOut, TweenMode::Forward, true);
};

/* DisappearEffect */

class DisappearEffect : public Effect {
public:
	DisappearEffect(float x, float y);

	void Update(float deltaTime) override;

	void Draw() override;

	bool IsFinished() const override;

private:
	const float _x;
	const float _y; 
	Animation _disappear = Animation("star_disappear");
};

/* ConfettiEffect */

class ConfettiEffect : public Effect {
public:
	ConfettiEffect(float duration);

	void Update(float deltaTime) override;

	void Draw() override;

	bool IsFinished() const override;

private:
	static constexpr int NUM_ANIMATIONS = 18;
	static constexpr int NUM_PARTICLES = 120;

	Animation _animations[NUM_ANIMATIONS] = {
		Animation("confetti"),
		Animation("confetti"),
		Animation("confetti"),
		Animation("confetti"),
		Animation("confetti"),
		Animation("confetti"),
		Animation("confetti"),
		Animation("confetti"),
		Animation("confetti"),
		Animation("confetti"),
		Animation("confetti"),
		Animation("confetti"),
		Animation("confetti"),
		Animation("confetti"),
		Animation("confetti"),
		Animation("confetti"),
		Animation("confetti"),
		Animation("confetti")
	};
	struct Particle {
		float alpha;
		Vector2 velocity;
		Vector2 position;
	} _particles[NUM_PARTICLES];
	Tweenf _alpha = Tweenf(1, 0, 1, TweenEasing::QuintIn, TweenMode::Forward, true);
};

} /* namespace Candy */

#endif /* CANDY_EFFECT_H */
