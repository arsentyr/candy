#ifndef CANDY_LEVEL_H
#define CANDY_LEVEL_H

#include <forward_list>
#include "Board.h"
#include "Button.h"
#include "Core.h"
#include "Progress.h"
#include "View.h"

namespace Candy {

/* Level */

class Level {
public:
	Level();

	void Reset();

	void TouchDown(float x, float y);

	void TouchMove(float x, float y);

	void TouchUp(float x, float y);

	void Update(float deltaTime);

	void Draw();

private:
	void BoardProgress(bool isFinished);

private:
	static constexpr int BOARD_ROWS = 7;
	static constexpr int BOARD_COLS = 6;
	static constexpr float SNOW_MIN_DURATION = 70;
	static constexpr float SNOW_MAX_DURATION = 100;
	static constexpr int SNOW_LARGE_COUNT = 5;
	static constexpr int SNOW_MEDIUM_COUNT = 6;
	static constexpr int SNOW_SMALL_COUNT = 7;

	Progress _progress;
	Button _button = Button(0, 0, "reset", [this]() { this->Reset(); });
	Board _board = Board(BOARD_ROWS, BOARD_COLS, [this](bool isFinished) { this->BoardProgress(isFinished); });
	std::forward_list<View*> _views = {&_board, &_progress, &_button};
	Sprite _background = Sprite("background");
	Sprite _pedestal = Sprite("pedestal");
	struct Snow {
		float x;
		Tweenf y;
		float amplitude;
		float frequency;
		float phase;
		float angle;
		float alpha;
		Texture* texture;
	} _snow[SNOW_LARGE_COUNT + SNOW_MEDIUM_COUNT + SNOW_SMALL_COUNT];
};

} /* namespace Candy */

#endif /* CANDY_LEVEL_H */
