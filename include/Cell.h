#ifndef CANDY_CELL_H
#define CANDY_CELL_H

#include <memory>
#include <vector>
#include "Chip.h"
#include "Core.h"

namespace Candy {

/* Cell */

class Cell : protected std::unique_ptr<Chip> {
public:
	using std::unique_ptr<Chip>::operator->;
	using std::unique_ptr<Chip>::operator*;

	explicit Cell(Chip* chip = nullptr, float x = 0, float y = 0);
	Cell(const Cell&) = delete;
	Cell(Cell&&) = default;

	Cell& operator=(const Cell&) = delete;
	Cell& operator=(Cell&&) = delete;

	~Cell() = default;

	void SetPosition(float x, float y);

	void Reset(Chip* chip, float x = 0, float y = 0);

	void To(Cell& cell);

	void Swap(Cell& cell);

	void Draw() const;

	bool IsMatchable() const;

private:
	Sprite _cell = Sprite("cell");
};

/* Cells */

typedef std::vector<std::vector<Cell>> Cells;

} /* namespace Candy */

#endif  /* CANDY_CELL_H */
