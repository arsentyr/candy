#ifndef CANDY_VIEW_H
#define CANDY_VIEW_H

namespace Candy {

/* View */

class View {
public:
	virtual ~View() = default;

	virtual bool TouchDown(float /*x*/, float /*y*/) { return false; }

	virtual bool TouchUp(float /*x*/, float /*y*/) { return false; }

	virtual bool TouchMove(float /*x*/, float /*y*/) { return false; }

	virtual void Update(float /*deltaTime*/) {}

	virtual void Draw() = 0; 

	void SetTouchable(bool isTouchable) { _isTouchable = isTouchable; }

	bool IsTouchable() const { return _isTouchable; }

private:
	bool _isTouchable = false;
};

} /* namespace Candy */

#endif /* CANDY_VIEW_H */
