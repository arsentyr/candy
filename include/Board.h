#ifndef CANDY_BOARD_H
#define CANDY_BOARD_H

#include <functional>
#include <list>
#include <memory>
#include "Cell.h"
#include "Chip.h"
#include "Core.h"
#include "Effect.h"
#include "OmNom.h"
#include "View.h"

namespace Candy {

/* Board */

class Board : public View {
public:
	typedef std::function<void (bool isFinished)> ProgressCallback;

public:
	Board() = delete;
	Board(int rows, int cols, const ProgressCallback& callback);
	Board(const Board&) = delete;
	Board(Board&&) = default;

	Board& operator=(const Board&) = delete;
	Board& operator=(Board&&) = delete;

	~Board() = default;

	void Reset();

public:	/* View Implementaion */
	bool TouchDown(float x, float y) override;

	bool TouchUp(float x, float y) override;

	bool TouchMove(float x, float y) override;

	void Update(float deltaTime) override;

	void Draw() override;

private:
	void Refresh(Cell& cell);

	void Refresh();

	void Match();

	bool HasMatch(int row, int col) const;

	void Swap(int row, int col);

private:
	static constexpr int MAX_STARS = 3;
	static constexpr float SWAP_DURATION = 0.3;
	static constexpr float DROP_DURATION = 0.8;
	static constexpr float REMOVE_DURATION = 0.7;
	static constexpr float CONFETTI_DURATION = 4;

	const int _rows = 0;
	const int _cols = 0;
	const IntRect _rect;
	const ProgressCallback _callback;

	int _numStars = 0;
	Cell _stars[MAX_STARS];
	Cells _cells;
	std::vector<Cell> _pool;
	std::list<std::unique_ptr<Effect>> _effects;

	int _panRow = -1;
	int _panCol = -1;
	bool _panStarted = false;

	OmNom _omNom;

	SoundSample& _sfxReset = Content.GetSoundSample("reset");
	SoundSample& _sfxMatch = Content.GetSoundSample("match");
	SoundSample& _sfxPingPong = Content.GetSoundSample("ping_pong");
	SoundSample& _sfxSwap = Content.GetSoundSample("swap");
	SoundSample& _sfxStar = Content.GetSoundSample("star");
};

} /* namespace Candy */

#endif /* CANDY_BOARD_H */
