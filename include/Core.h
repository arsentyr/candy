#ifndef CORE_H
#define CORE_H

#include "Animation/Animation.h"
#include "Animation/Tween.h"
#include "Audio/Sound.h"
#include "Audio/SoundSample.h"
#include "Assert.h"
#include "BuildConfig.h"
#include "Content/ContentManager.h"
#include "Graphics/Graphics.h"
#include "Graphics/Sprite.h"
#include "Graphics/Texture.h"
#include "Math/Math.h"
#include "Math/Matrix4.h"
#include "Math/Rectangle.h"
#include "Math/Vector2.h"
#include "Utils/Loggable.h"
#include "Utils/Timer.h"
#include "Utils/Random.h"

namespace Candy {

static constexpr float LOGICAL_WIDTH = 480;
static constexpr float LOGICAL_HEIGHT = 800;
// Animation
using Core::Animation;
using Core::EasingBack;
using Core::TweenEasing;
using Core::TweenMode;
using Core::TweenEquation;
typedef Core::Tween<float> Tweenf;
typedef Core::Tween<Core::Vector2> Tweenv;
// Audio
using Core::Sfx;
using Core::SoundSample;
// BuildConfig
using Core::BuildConfig;
// Content
using Core::Content;
// Math
using Core::IntRect;
using Core::Matrix4;
using Core::Vector2;
using Core::Min;
using Core::Max;
using Core::Cos;
using Core::Sin;
// Graphics
using Core::BlendMode;
using Core::Color4f;
using Core::Gfx;
using Core::MatrixMode;
using Core::Sprite;
using Core::Texture;
// Utils
using Core::Log;
using Core::Timer;
extern Core::Random& Rand;

} /* namespace Candy  */

#endif /* CORE_H */
