#ifndef CORE_RECTANGLE_H
#define CORE_RECTANGLE_H

namespace Core {

/* Rectangle */

template<typename T>
class Rectangle final {
public:
	T x;
	T y;
	T width;
	T height;

public:
	constexpr Rectangle() noexcept
			: x(0)
			, y(0)
			, width(0)
			, height(0) {
	}

	constexpr Rectangle(T x, T y, T width = 0, T height = 0) noexcept
			: x(x)
			, y(y)
			, width(width)
			, height(height) {
	}

	constexpr T Left() const {
		return x;
	}

	constexpr T Right() const {
		return x + width;
	}

	constexpr T Bottom() const {
		return y;
	}

	constexpr T Top() const {
		return y + height;
	}
};

typedef Rectangle<int> IntRect;

} /* namespace Core */

#endif /* CORE_RECTANGLE_H */
