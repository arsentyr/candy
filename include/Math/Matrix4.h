#ifndef CORE_MATRIX4_H
#define CORE_MATRIX4_H

namespace Core {

/* Matrix4 */

class Matrix4 final {
public:
	static constexpr Matrix4 Zero() {
		return Matrix4(
			0, 0, 0, 0,
			0, 0, 0, 0,
			0, 0, 0, 0,
			0, 0, 0, 0
		);
	}

	static constexpr Matrix4 Identity() {
		return Matrix4(
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		);
	}

public:
	float x0, x1, x2, x3;
	float y0, y1, y2, y3;
	float z0, z1, z2, z3;
	float w0, w1, w2, w3;

public:
	constexpr Matrix4() noexcept
			: x0(0), x1(0), x2(0), x3(0)
			, y0(0), y1(0), y2(0), y3(0)
			, z0(0), z1(0), z2(0), z3(0)
			, w0(0), w1(0), w2(0), w3(0) {
	}

	constexpr Matrix4(float x0, float x1, float x2, float x3,
					  float y0, float y1, float y2, float y3,
					  float z0, float z1, float z2, float z3,
					  float w0, float w1, float w2, float w3) noexcept
			: x0(x0), x1(x1), x2(x2), x3(x3)
			, y0(y0), y1(y1), y2(y2), y3(y3)
			, z0(z0), z1(z1), z2(z2), z3(z3)
			, w0(w0), w1(w1), w2(w2), w3(w3) {
	}

	operator const float* () const {
		return &x0;
	}
};

Matrix4 operator*(const Matrix4& mat1, const Matrix4& mat2);

} /* namespace Core */

#endif /* CORE_MATRIX4_H */
