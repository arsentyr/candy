#ifndef CORE_VECTOR2_H
#define CORE_VECTOR2_H

namespace Core {

/* Vector2 */

class Vector2 final {
public:
	float x;
	float y;

public:
	constexpr Vector2() noexcept
			: x(0)
			, y(0) {
	}

	constexpr Vector2(float x, float y) noexcept
			: x(x)
			, y(y) {
	}

	Vector2& operator+=(const Vector2& vec) {
		x += vec.x;
		y += vec.y;
		return *this;
	}

	Vector2& operator-=(const Vector2& vec) {
		x -= vec.x;
		y -= vec.y;
		return *this;
	}

	float Length() const {
		return Sqrt(x * x + y * y);
	}

	Vector2 Normalized() const {
		ASSERT(Length() != 0);
		const auto length = Length();
		return (length > 0) ? Vector2(x / length, y / length) : *this;
	}
};

inline constexpr Vector2 operator+(const Vector2& left, const Vector2& right) {
	return Vector2(left.x + right.x, left.y + right.y);
}

inline constexpr Vector2 operator-(const Vector2& left, const Vector2& right) {
	return Vector2(left.x - right.x, left.y - right.y);
}

inline constexpr Vector2 operator*(const Vector2& vec, float scalar) {
	return Vector2(vec.x * scalar, vec.y * scalar);
}

} /* namespace Core */

#endif /* CORE_VECTOR2_H */
