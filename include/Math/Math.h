#ifndef CORE_MATH_H
#define CORE_MATH_H

#include <cmath>

#define PI 3.14159265358979323846
#define PI_2 (PI / 2)

namespace Core {

template<typename T>
inline T Min(T left, T right) {
    return (left < right) ? left : right;
}

template<typename T>
inline T Max(T left, T right) {
    return (left > right) ? left : right;
}

template<typename T>
inline T Cos(T radians) {
    return cos(radians);
}

template<typename T>
inline T Sin(T radians) {
    return sin(radians);
}

template<typename T>
inline T Sqrt(T value) {
    return sqrt(value);
}

template<typename T>
inline T RadToDeg(T radians) {
    return radians * 180 / PI;
}

template<typename T>
inline T DegToRad(T degrees) {
    return degrees * PI / 180;
}

} /* namespace Core */

#endif /* CORE_MATH_H */
