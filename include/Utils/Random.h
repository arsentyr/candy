#ifndef CORE_RANDOM_H
#define CORE_RANDOM_H

#include <chrono>
#include <random>

namespace Core {

/* Random */

class Random final {
public:
	Random() = default;
	Random(const Random&) = delete;
	Random(Random&&) = default;

	Random& operator=(const Random&) = delete;
	Random& operator=(Random&&) = default;

	~Random() = default;

	bool NextBool();

	int NextInt(int min, int max);

	float NextFloat(float min, float max);

private:
	typedef std::default_random_engine RandomEngine;
	typedef std::uniform_int_distribution<int> IntDistribution;
	typedef std::uniform_real_distribution<float> FloatDistribution;

	RandomEngine _generator = RandomEngine(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()));
	IntDistribution _intDistribution;
	FloatDistribution _floatDistribution;
};

} /* namespace Core */

#endif /* CORE_RANDOM_H */
