#ifndef CORE_LOGGABLE_H
#define CORE_LOGGABLE_H

namespace Core {

/* Loggable */

class Loggable {
public:
	Loggable() = default;
	Loggable(const Loggable&) = delete;
	Loggable(Loggable&&) = delete;

	Loggable& operator=(const Loggable&) = delete;
	Loggable& operator=(Loggable&&) = delete;

	virtual ~Loggable() = default;

	virtual void Debug(const char* format, ...) = 0;

	virtual void Warning(const char* format, ...) = 0;

	virtual void Error(const char* format, ...) = 0;
};

extern Loggable& Log;

} /* namespace Core */

#endif /* CORE_LOGGABLE_H */
