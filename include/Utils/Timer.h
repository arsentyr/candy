#ifndef CORE_TIMER_H
#define CORE_TIMER_H

#include <chrono>

namespace Core {

/* Timer */

class Timer final {
public:
	void Start();

	void Stop();
	
	float GetDeltaTime();

private:
	bool _started = false;
	std::chrono::time_point<std::chrono::steady_clock> _time;
};

} /* namespace Core */

#endif /* CORE_TIMER_H */
