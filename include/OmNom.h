#ifndef CANDY_OM_NOM_H
#define CANDY_OM_NOM_H

#include "Core.h"

namespace Candy {

enum class OmNomMode {
	Eat,
	Crew,
	Sits,
	Shake
};

/* OmNom */

class OmNom {
public:
	OmNom();

	void SetPosition(float x, float y);

	float Left() const;

	float Bottom() const;

	void SetMode(OmNomMode mode);

	void Update(float deltaTime);

	void Draw();

private:
	OmNomMode _mode;
	Animation _animation = Animation("omnom_first");
	float _x = 0;
	float _y = 0;
	float _duration = 0;

	SoundSample& _sfxCrew = Content.GetSoundSample("crew");
	SoundSample& _sfxEat = Content.GetSoundSample("eat");
};

} /* namespace Candy */

#endif /* CANDY_OM_NOM_H */
