#ifndef CANDY_APPLICATION_H
#define CANDY_APPLICATION_H

#include "Core.h"
#include "Level.h"

namespace Candy {

/* Application */

class Application {
public:
	static Application& Instance();

public:
	Application(const Application&) = delete;
	Application(Application&&) = delete;

	Application& operator=(const Application&) = delete;
	Application& operator=(Application&&) = delete;

	~Application() = default;

	void Initialize();

	void Reload();

	void Resize(int widht, int height);

	void Resume();

	void Loop();

	void Pause();

	void Release();

	void TouchDown(float x, float y);

	void TouchMove(float x, float y);

	void TouchUp(float x, float y);

private:
	Application() = default;

	void TransformTouch(float& x, float& y);

private:
	enum class State {
		Uninitialized,
		Initialized,
		Resumed,
		Paused
	};

	float _scale = 1;
	float _halfWidth = 0;
	float _halfHeight = 0;
	State _state = State::Uninitialized;
	Timer _timer;
	Level _level;
};

} /* namespace Candy */

#endif /* CANDY_APPLICATION_H */
