#ifndef CANDY_BUTTON_H
#define CANDY_BUTTON_H

#include <functional>
#include "Core.h"
#include "View.h"

namespace Candy {

/* Button */

class Button : public View {
public:
	typedef std::function<void ()> ClickCallback;

public:
	Button(float x, float y, const char* name, const ClickCallback& callback);

	void SetPosition(float x, float y);

	int Width() const;

	int Height() const;

public:	/* View Implementaion */
	void Draw() override;

	bool TouchDown(float x, float y) override;

	bool TouchUp(float x, float y) override;

	bool TouchMove(float x, float y) override;

private:
	bool Contains(float x, float y) const;

private:
	float _x;
	float _y;
	const Texture& _texture;
	ClickCallback _callback;
	bool _hasTouch = false;
};

} /* namespace Candy */

#endif /* CANDY_BUTTON_H */
