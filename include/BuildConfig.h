#ifndef CORE_BUILD_CONFIG_H
#define CORE_BUILD_CONFIG_H

namespace Core {

/* BuildConfig */

class BuildConfig final {
public:
#ifdef NDEBUG
	static constexpr bool DEBUG = false;
	static constexpr bool LOGGABLE_DEBUG = false;
	static constexpr bool LOGGABLE_WARNING = true;
	static constexpr bool LOGGABLE_ERROR = true;
#else
	static constexpr bool DEBUG = true;
	static constexpr bool LOGGABLE_DEBUG = true;
	static constexpr bool LOGGABLE_WARNING = true;
	static constexpr bool LOGGABLE_ERROR = true;
#endif /* NDEBUG */
	static constexpr int MAX_BITS_PER_TEXEL = 8;
	static constexpr int TEXTURE_MAX_WIDTH = 1024;
	static constexpr int TEXTURE_MAX_HEIGHT = 2048;
	static constexpr int LOADER_BUFFER_SIZE = TEXTURE_MAX_WIDTH * TEXTURE_MAX_HEIGHT * MAX_BITS_PER_TEXEL;

public:
	BuildConfig() = delete;
	BuildConfig(const BuildConfig&) = delete;
	BuildConfig(BuildConfig&&) = delete;

	BuildConfig& operator=(const BuildConfig&) = delete;
	BuildConfig& operator=(BuildConfig&&) = delete;

	~BuildConfig() = delete;
};

} /* namespace Core */

#endif /* CORE_BUILD_CONFIG_H */
