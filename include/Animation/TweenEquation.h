#ifndef CORE_TWEEN_EQUATION_H
#define CORE_TWEEN_EQUATION_H

#include <functional>
#include "Assert.h"

namespace Core {

enum class TweenEasing {
	Linear,
	QuadIn,
	QuadOut,
	QuadInOut,
	QuintIn,
	BackOut
};

/* EasingBack */

class EasingBack final {
public:
	explicit EasingBack(float back = 0) noexcept
			: _back(back) {
		ASSERT(back > 0);
	}

	static float EaseOut(float t, float s) {
		ASSERT(t >= 0 && t <= 1 && s > 0);
		t -= 1;
		return t * t * ((s + 1) * t + s) + 1;
	}

	float operator()(float t) const {
		return EaseOut(t, _back);
	}

private:
	const float _back;
};

/* TweenEquation */

class TweenEquation final {
public:
	TweenEquation(TweenEasing easing);

	TweenEquation(EasingBack&& easingBack);

	float operator()(float t) const {
		return _equation(t);
	}

private:
	std::function<float (float)> _equation;
};

} /* namespace Core */

#endif /* CORE_TWEEN_EQUATION_H */
