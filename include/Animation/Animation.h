#ifndef CORE_ANIMATION_H
#define CORE_ANIMATION_H

#include <vector>
#include <string>
#include <unordered_map>
#include "Animation/Tween.h"
#include "Graphics/Texture.h"

namespace Core {

/* AnimationFrame */

struct AnimationFrame {
	unsigned int width;
	unsigned int height;
	float u0;
	float v0;
	float u1;
	float v1;
};

/* AnimationAction */

struct AnimationAction {
	unsigned int initial;
	unsigned int count;
	float duration;
	TweenMode mode;
};

/* AnimationContent */

struct AnimationContent {
	std::string name;
	std::vector<AnimationFrame> frames;
	std::unordered_map<std::string, AnimationAction> actions;
};

/* Animation */

class Animation final {
public:
	explicit Animation(const std::string& content, float x = 0, float y = 0);
	Animation(const Animation&) = delete;
	Animation(Animation&&) = default;

	Animation& operator=(const Animation&) = delete;
	Animation& operator=(Animation&&) = delete;

	~Animation() = default;

	void SetContent(const std::string& content);

	void SetPosition(float x, float y);

	float Left() const;

	float Right() const;

	float Bottom() const;

	int Width() const;

	int Height() const;

	float Duration() const;

	void Play(const std::string& action);

	void Pause();

	void Resume();

	bool Update(float deltaTime);

	void Draw() const;

	bool IsPlaying() const;

	bool IsFinished() const;

private:
	float _x;
	float _y;
	unsigned int _index;
	Tween<unsigned int> _tween;
	AnimationContent* _content;
	Texture* _texture;
};

} /* namespace Core */

#endif /* CORE_ANIMATION_H */
