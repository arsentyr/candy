#ifndef CORE_TWEEN_H
#define CORE_TWEEN_H

#include <cmath>
#include <limits>
#include "Assert.h"
#include "Animation/TweenEquation.h"

namespace Core {

enum class TweenMode {
	Forward,
	LoopForward,
	LoopPingPong
};

/* Tween */

template<typename T>
class Tween final {
public:
	Tween() = default;

	Tween(const T& initial, const T& end, float duration, TweenEquation equation, TweenMode mode = TweenMode::Forward, bool isTweening = false)
			: _value(initial)
			, _equation(equation) {
		SetInitial(initial);
		SetEnd(end);
		SetDuration(duration);
		SetTime(0);
		SetMode(mode);
		if (isTweening) {
			Resume();
		}
	}

	Tween(const T& initial, const T& end, float duration, TweenEasing easing = TweenEasing::Linear, TweenMode mode = TweenMode::Forward, bool isTweening = false)
			: Tween(initial, end, duration, TweenEquation(easing), mode, isTweening) {
	}

	const T& Value() const {
		return _value;
	}

	operator T() const noexcept {
		return _value;
	}

	void SetInitial(const T& initial) {
		_change = _change + _initial - initial;
		_initial = initial;
	}

	T GetInitial() const {
		return _initial;
	}

	void SetEnd(const T& end) {
		_change = end - _initial;
	}

	T GetEnd() const {
		return _initial + _change;
	}	

	void SetEquation(const TweenEquation& equation) {
		_equation = equation;
	}

	void SetDuration(float duration) {
		ASSERT(duration > 0);
		_duration = (duration > 0) ? duration : std::numeric_limits<float>::min();
	}

	float GetDuration() const {
		return _duration;
	}

	void SetTime(float time) {
		ASSERT(time >= 0 && time <= _duration);
		_time = (time < 0) ? 0 : ((time > _duration) ? _duration : time);
		if (_time < _duration && _state == TweenState::Finished) {
			_state = TweenState::Paused;
		}
	}

	float GetTime() const {
		return _time;
	}

	void SetMode(TweenMode mode) {
		_mode = mode;
		if (_state == TweenState::Finished
				&& (_mode == TweenMode::LoopForward || _mode == TweenMode::LoopPingPong)) {
			_state = TweenState::Paused;
		}
	}

	TweenMode GetMode() const {
		return _mode;
	}

	void Resume() {
		ASSERT(_state != TweenState::Finished);
		if (_state != TweenState::Finished) {
			_state = TweenState::Tweening;
			Update(0);
		}
	}

	void Pause() {
		ASSERT(_state != TweenState::Finished);
		if (_state != TweenState::Finished) {
			_state = TweenState::Paused;
		}
	}

	bool Update(float deltaTime) {
		ASSERT(deltaTime >= 0);
		if (_state != TweenState::Tweening) {
			return false;
		}

		_time += deltaTime;
		switch (_mode) {
		case TweenMode::Forward:
			if (_time < 0) {
				_time = 0;
				_state = TweenState::Paused;
			}
			else if (_time >= _duration) {
				_time = _duration;
				_state = TweenState::Finished;
			}
			break;
		case TweenMode::LoopForward:
			if (_time < 0) {
				_time -= _time;
				if (_time > _duration) {
					_time = fmod(_time, _duration);
				}
			}
			else if (_time > _duration) {
				_time -= _duration;
				if (_time > _duration) {
					_time = fmod(_time, _duration);
				}
			}
			break;
		case TweenMode::LoopPingPong:
			if (_time < 0) {
				_time = -_time;
				if (_time > _duration) {
					const auto count = static_cast<int>(_time / _duration);
					_time -= count * _duration;
					if (count % 2 != 0) {
						_initial = _initial + _change;
						_change = _change - _change - _change;
					}
				}
			}
			else if (_time > _duration) {
				_time -= _duration;
				if (_time <= _duration) {
					_initial = _initial + _change;
					_change = _change - _change - _change;
				}
				else {
					const auto count = static_cast<int>(_time / _duration);
					_time -= count * _duration;
					if (count % 2 == 0) {
						_initial = _initial + _change;
						_change = _change - _change - _change;
					}
				}
			}
			break;
		}

		_value = _initial + _change * _equation(_time / _duration);
		return true;
	}

	bool IsTweening() const {
		return _state == TweenState::Tweening;
	}

	bool IsPaused() const {
		return _state == TweenState::Paused;
	}

	bool IsFinished() const {
		return _state == TweenState::Finished;
	}

private:
	enum class TweenState {
		Tweening,
		Paused,
		Finished
	};

	T _value;
	T _initial;
	T _change = _initial - _initial;
	float _duration = std::numeric_limits<float>::min();
	float _time = _duration;
	TweenMode _mode =  TweenMode::Forward;
	TweenState _state = TweenState::Finished;
	TweenEquation _equation = TweenEquation(TweenEasing::Linear);
};

} /* namespace Core */

#endif /* CORE_TWEEN_H */
