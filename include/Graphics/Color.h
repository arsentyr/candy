#ifndef CORE_COLOR_H
#define CORE_COLOR_H

#include <type_traits>
#include "Assert.h"

namespace Core {

/* Color4f */

#define CHECK_COLOR4F(color) ASSERT(color.red >= 0 && color.red <= 1 && color.green >= 0 && color.green <= 1 && color.blue >= 0 && color.blue <= 1 && color.alpha >= 0 && color.alpha <= 1)

class Color4f final {
public:
	static constexpr Color4f White() {
		return Create(1, 1, 1, 1);
	}

public:
	float red;
	float green;
	float blue;
	float alpha;

public:
	static constexpr Color4f Create(float red, float green, float blue, float alpha = 1) {
		return {red, green, blue, alpha};
	}
};
static_assert(std::is_pod<Color4f>::value, "Color4f must be a POD type.");

} /* namespace Core */

#endif /* CORE_COLOR_H */
