#ifndef CORE_MATRIX_STACK_H
#define CORE_MATRIX_STACK_H

#include <vector>
#include "Math/Matrix4.h"

namespace Core {

/* MatrixStack */

class MatrixStack final {
public:
	explicit MatrixStack(unsigned int capacity);

	void PushMatrix();

	void PopMatrix();

	const Matrix4& TopMatrix() const;

	void LoadIdentity();

	void LoadMatrix(const Matrix4& mat);
	void LoadMatrix(Matrix4&& mat);

	void MultMatrix(const Matrix4& mat);
	void MultMatrix(Matrix4&& mat);

	void Reset();

	unsigned int Depth() const;

	void Scale(float scaleX, float scaleY, float scaleZ);

	void RotateZ(float radians);

	void Translate(float deltaX, float deltaY, float deltaZ);

private:
	std::vector<Matrix4> _stack;
};

} /* namespace Core */

#endif /* CORE_MATRIX_STACK_H */
