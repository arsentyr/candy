#ifndef CORE_SHADER_PROGRAM_H
#define CORE_SHADER_PROGRAM_H

#include "Graphics/GLES2/GLES2.h"

namespace Core {

/* ShaderProgram */

class ShaderProgram {
public:
	ShaderProgram() noexcept = default;
	ShaderProgram(const ShaderProgram&) = delete;
	ShaderProgram(ShaderProgram&&) = delete;

	ShaderProgram& operator=(const ShaderProgram&) = delete;
	ShaderProgram& operator=(ShaderProgram&&) = delete;

	virtual ~ShaderProgram();

	GLuint Name() const;

public: /* Life cycle */
	void Create(const GLchar* sourceVertexShader, const GLchar* sourceFragmentShader);

	void Link();

	void Delete();

	void Lose();

	bool IsProgram() const;

public: /* Attributes */
	void BindAttributeLocation(GLuint index, const GLchar* name);

	static void SetVertexAttributePointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* pointer);

	static void EnableVertexAttributeArray(GLuint index);

public: /* Uniforms */
	GLint GetUniformLocation(const GLchar* name) const;

	static void SetUniform4fv(GLint location, GLsizei count, const GLfloat* fvalues);

	static void SetUniformMatrix4fv(GLint location, GLsizei count, const GLfloat* fvalues);

private:
	static constexpr GLuint UNKNOWN_PROGRAM = 0;
	static constexpr GLuint UNKNOWN_SHADER = 0;

	GLuint _program = UNKNOWN_PROGRAM;
	GLuint _vertexShader = UNKNOWN_SHADER;
	GLuint _fragmentShader = UNKNOWN_SHADER;
};

} /* namespace Core */

#endif /* CORE_SHADER_PROGRAM_H */
