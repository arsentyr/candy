#ifndef CORE_TEXTURE_H
#define CORE_TEXTURE_H

#include "Graphics/GLES2/GLES2.h"

namespace Core {

/* TextureVertex */

struct TextureVertex {
	float x;
	float y;
	float z;
	float u;
	float v;
};

/* Texture */

class Texture final {
public:
	Texture() noexcept = default;
	Texture(const Texture&) = delete;
	Texture(Texture&& texture) = default;

	Texture& operator=(const Texture&) = delete;
	Texture& operator=(Texture&& texture) = delete;

	~Texture();

	int Width() const;

	int Height() const;

	void Load(const void* data, int width, int height);

	void Unload();

	void Lose();

	bool IsLoaded() const;

	void Draw(float x, float y, float z = 0) const;

	void Draw(float x, float y, float z, float width, float height, float u0, float v0, float u1, float v1) const;

private:
	bool IsTexture() const;

private:
	static constexpr GLuint UNKNOWN_TEXTURE = 0;

	int _width = 0;
	int _height = 0;
	GLuint _texture = UNKNOWN_TEXTURE;
};

} /* namespace Core */

#endif /* CORE_TEXTURE_H */ 
