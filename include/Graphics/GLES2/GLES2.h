#ifndef CORE_GLES2_H
#define CORE_GLES2_H

#include <GLES2/gl2.h>

#ifdef NDEBUG
#	define CHECK_GL_ERROR() ((void)0)
#else
#	include "Assert.h"
#	define CHECK_GL_ERROR() do { auto error = glGetError(); ASSERTF(error == GL_NO_ERROR, "GL error %x.", error); } while (false)
#endif /* NDEBUG */

#endif /* CORE_GLES2_H */
