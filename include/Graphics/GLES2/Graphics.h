#ifndef CORE_GRAPHICS_H
#define CORE_GRAPHICS_H

#include "Graphics/Color.h"
#include "Graphics/MatrixStack.h"
#include "Graphics/GLES2/GLES2.h"
#include "Graphics/GLES2/ShaderProgram.h"

namespace Core {

enum class MatrixMode {
	ModelView,
	Projection
};

enum class BlendMode {
	Alpha,
	Add
};

/* Graphics */

class Graphics final {
public:
	Graphics() = default;
	Graphics(const Graphics&) = delete;
	Graphics(Graphics&&) = delete;

	Graphics& operator=(const Graphics&) = delete;
	Graphics& operator=(Graphics&&) = delete;
	
	~Graphics();

public: /* Life cycle */
	void Initialize();

	void Reload();

	void Release();

public: /* Transforms */
	void SetViewport(int x, int y, int width, int height);

	void SetOrtho(float xLeft, float xRight, float yBottom, float yTop, float zNear, float zFar);

	void SetMatrixMode(MatrixMode matrixMode);

	void ResetMatrixMode();

	void PushMatrix();
	
	void PopMatrix();

	void Scale(float scaleX, float scaleY, float scaleZ = 1);

	void Rotate(float angleZ);

	void Translate(float deltaX, float deltaY, float deltaZ = 0);

public: /* Drawing */
	void Begin();
	
	void End();

	void SetBlendMode(BlendMode blendMode);

	void ResetBlendMode();

	void SetColor(float red, float green, float blue, float alpha = 1);

	void SetColor(const Color4f& color);

	void ResetColor();

	void DrawQuad(const void* vertices, int count);

private:
	void LoadShaderProgram();

	void UnloadShaderProgram();

	void MvpMatrixChanged();

	void ColorChanged();

private:
	enum class State {
		Uninitialized,
		Initialized,
		Framed
	}; 

	State _state = State::Uninitialized;

private: /* Vertex Attributes */
	// Model-View Projection Matrix
	MatrixMode _matrixMode = MatrixMode::ModelView;
	MatrixStack _modelViewStack = MatrixStack(4);
	MatrixStack _projectionStack = MatrixStack(2);
	Matrix4 _projectionMatrix = Matrix4::Zero();
	bool _isMvpMatrixChanged = false;
	// Color
	Color4f _color = Color4f::White();

private: /* ShaderProgram */
	static constexpr GLint UNKNOWN_UNIFORM_LOCATION = -1;
	static constexpr GLint ATTRIBUTE_POSITION_INDEX = 0;
	static constexpr GLint ATTRIBUTE_TEX_COORD_INDEX = 1;

	ShaderProgram _shaderProgram;
	GLint _uniformMvpMatrixLocation = UNKNOWN_UNIFORM_LOCATION;
	GLint _uniformColorLocation = UNKNOWN_UNIFORM_LOCATION;
};

extern Graphics& Gfx;

} /* namespace Core */

#endif /* CORE_GRAPHICS_H */
