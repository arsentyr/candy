#ifndef CORE_SPRITE_H
#define CORE_SPRITE_H

#include "Graphics/Texture.h"

namespace Core {

/* Sprite */

class Sprite final {
public:
	explicit Sprite(const char* name, float x = 0, float y = 0);
	Sprite(const Sprite&) = delete;
	Sprite(Sprite&&) = default;

	Sprite& operator=(const Sprite&) = delete;
	Sprite& operator=(Sprite&&) = delete;

	~Sprite() = default;

	void SetTexture(const char* name);

	void SetPosition(float x, float y);

	float Left() const;

	float Right() const;

	float Bottom() const;

	float Top() const;

	int Width() const;

	int Height() const;

	void Draw() const;

private:
	float _x = 0;
	float _y = 0;
	const Texture* _texture = nullptr;
};

} /* namespace Core */

#endif /* CORE_SPRITE_H */
