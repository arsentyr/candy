#include "Math/Matrix4.h"

namespace Core {

Matrix4 operator*(const Matrix4& mat1, const Matrix4& mat2) {
	return Matrix4(
		mat1.x0 * mat2.x0 + mat1.x1 * mat2.y0 + mat1.x2 * mat2.z0 + mat1.x3 * mat2.w0,
		mat1.x0 * mat2.x1 + mat1.x1 * mat2.y1 + mat1.x2 * mat2.z1 + mat1.x3 * mat2.w1,
		mat1.x0 * mat2.x2 + mat1.x1 * mat2.y2 + mat1.x2 * mat2.z2 + mat1.x3 * mat2.w2,
		mat1.x0 * mat2.x3 + mat1.x1 * mat2.y3 + mat1.x2 * mat2.z3 + mat1.x3 * mat2.w3,
		mat1.y0 * mat2.x0 + mat1.y1 * mat2.y0 + mat1.y2 * mat2.z0 + mat1.y3 * mat2.w0,
		mat1.y0 * mat2.x1 + mat1.y1 * mat2.y1 + mat1.y2 * mat2.z1 + mat1.y3 * mat2.w1,
		mat1.y0 * mat2.x2 + mat1.y1 * mat2.y2 + mat1.y2 * mat2.z2 + mat1.y3 * mat2.w2,
		mat1.y0 * mat2.x3 + mat1.y1 * mat2.y3 + mat1.y2 * mat2.z3 + mat1.y3 * mat2.w3,
		mat1.z0 * mat2.x0 + mat1.z1 * mat2.y0 + mat1.z2 * mat2.z0 + mat1.z3 * mat2.w0,
		mat1.z0 * mat2.x1 + mat1.z1 * mat2.y1 + mat1.z2 * mat2.z1 + mat1.z3 * mat2.w1,
		mat1.z0 * mat2.x2 + mat1.z1 * mat2.y2 + mat1.z2 * mat2.z2 + mat1.z3 * mat2.w2,
		mat1.z0 * mat2.x3 + mat1.z1 * mat2.y3 + mat1.z2 * mat2.z3 + mat1.z3 * mat2.w3,
		mat1.w0 * mat2.x0 + mat1.w1 * mat2.y0 + mat1.w2 * mat2.z0 + mat1.w3 * mat2.w0,
		mat1.w0 * mat2.x1 + mat1.w1 * mat2.y1 + mat1.w2 * mat2.z1 + mat1.w3 * mat2.w1,
		mat1.w0 * mat2.x2 + mat1.w1 * mat2.y2 + mat1.w2 * mat2.z2 + mat1.w3 * mat2.w2,
		mat1.w0 * mat2.x3 + mat1.w1 * mat2.y3 + mat1.w2 * mat2.z3 + mat1.w3 * mat2.w3
	);
}

} /* namespace Core */
