#include "Level.h"

namespace Candy {

Level::Level() {
	for (auto idx = 0; idx < SNOW_LARGE_COUNT; ++idx) {
		_snow[idx].texture = &Content.GetTexture("snow_large");
	}
	for (auto idx = SNOW_LARGE_COUNT; idx < (SNOW_LARGE_COUNT + SNOW_MEDIUM_COUNT); ++idx) {
		_snow[idx].texture = &Content.GetTexture("snow_medium");
	}
	for (auto idx = (SNOW_LARGE_COUNT + SNOW_MEDIUM_COUNT); idx < (SNOW_LARGE_COUNT + SNOW_MEDIUM_COUNT + SNOW_SMALL_COUNT); ++idx) {
		_snow[idx].texture = &Content.GetTexture("snow_small");
	}
}

void Level::Reset() {
	_board.SetTouchable(true);
	_board.Reset();
	
	_progress.SetTouchable(false);
	_progress.SetState(ProgressState::None);
	_progress.SetPosition((LOGICAL_WIDTH * 0.01), (LOGICAL_HEIGHT * 0.99 - _progress.Height()));
	
	_button.SetTouchable(true);
	_button.SetPosition((LOGICAL_WIDTH - _button.Width()), (LOGICAL_HEIGHT - _button.Height()));
	
	_background.SetPosition((LOGICAL_WIDTH - _background.Width()) / 2.f, (LOGICAL_HEIGHT - _background.Height()) / 2.f);
	_pedestal.SetPosition((LOGICAL_WIDTH * 0.5f - _pedestal.Width() / 2.f), 0);
	
	for (auto& snow : _snow) {
		snow.y.SetInitial(Rand.NextFloat((LOGICAL_HEIGHT + snow.texture->Height()), (LOGICAL_HEIGHT + LOGICAL_HEIGHT)));
		snow.y.SetEnd(-snow.texture->Height());
		snow.y.SetTime(0);
		snow.y.SetDuration(Rand.NextFloat(SNOW_MIN_DURATION, SNOW_MAX_DURATION) * (snow.y.GetInitial() - snow.y.GetEnd()) / LOGICAL_HEIGHT);
		snow.y.Resume();
		snow.x = Rand.NextFloat(0, LOGICAL_WIDTH);
		snow.amplitude = Rand.NextFloat((LOGICAL_WIDTH * 0.075), (LOGICAL_WIDTH * 0.175));
		snow.frequency = 20 / snow.y.GetDuration();
		snow.phase = Rand.NextFloat(0, (2 * PI));
		snow.angle = Rand.NextFloat(100, 180);
		snow.alpha = Rand.NextFloat(0.5, 1);
		if (Rand.NextBool()) {
			snow.angle = -snow.angle;  
		}
	}
}

void Level::TouchDown(float x, float y) {
	for (const auto& view : _views) {
		if (view->IsTouchable() && view->TouchDown(x, y)) {
			break;
		}
	}
}

void Level::TouchMove(float x, float y) {
	for (const auto& view : _views) {
		if (view->IsTouchable() && view->TouchMove(x, y)) {
			break;
		}
	}
}

void Level::TouchUp(float x, float y) {
	for (const auto& view : _views) {
		if (view->IsTouchable() &&  view->TouchUp(x, y)) {
			break;
		}
	}
}

void Level::Update(float deltaTime) {
	for (auto& snow : _snow) {
		snow.y.Update(deltaTime);
		if (snow.y.IsFinished()) {
			snow.x = Rand.NextFloat(0, LOGICAL_WIDTH);
			snow.y.SetInitial(LOGICAL_HEIGHT + snow.texture->Height());
			snow.y.SetEnd(-snow.texture->Height());
			snow.y.SetTime(0);
			snow.y.SetDuration(Rand.NextFloat(SNOW_MIN_DURATION, SNOW_MAX_DURATION));
			snow.y.Resume();
		}
	}
	for (const auto& view : _views) {
		view->Update(deltaTime);
	}
}

void Level::Draw() {
	_background.Draw();
	Gfx.SetMatrixMode(MatrixMode::Projection);
	for (const auto& snow : _snow) {
		const auto sine = Sin(snow.frequency * snow.y.GetTime() + snow.phase);
		const auto halfWidth = snow.texture->Width() / 2.f;
		const auto halfHeight = snow.texture->Height() / 2.f;
		Gfx.PushMatrix();
		Gfx.Translate(
			snow.x + snow.amplitude * sine + halfWidth,
			snow.y + halfHeight
		); 
		Gfx.Rotate(snow.angle * sine);
		Gfx.SetColor(1, 1, 1, snow.alpha);
		snow.texture->Draw(-halfWidth, -halfHeight);
		Gfx.PopMatrix();
	}
	Gfx.ResetColor();
	Gfx.ResetMatrixMode();
	_pedestal.Draw();
	for (const auto& view : _views) {
		view->Draw();
	}
}

void Level::BoardProgress(bool isFinished) {
	if (isFinished) {
		Reset();
	}
	else {
		_progress.Increase();
	}
}

} /* namespace Candy */
