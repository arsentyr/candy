#include "Content/Image.h"

#include <vector>
#include <png.h>
#include "Assert.h"
#include "Utils/Loggable.h"

namespace Core { namespace {

void PngWarning(png_structp ptr, png_const_charp msg) {
	auto reader = static_cast<DataReader*>(png_get_io_ptr(ptr));
	Log.Warning("File %s, libpng warning: %s.", reader->Name(), msg);
}

void PngError(png_structp ptr, png_const_charp msg) {
	auto reader = static_cast<DataReader*>(png_get_io_ptr(ptr));
	Log.Error("File %s, libpng error: %s.", reader->Name(), msg);
}

void PngRead(png_structp ptr, png_bytep buffer, png_size_t count) {
	auto reader = static_cast<DataReader*>(png_get_io_ptr(ptr));
	reader->Read(buffer, count);
}

} /* anonymous namespace */

void DecodePng(DataReader& reader, void* buffer, unsigned int* width, unsigned int* height) {
	ASSERT(reader.IsOpen());
	ASSERT(buffer != nullptr && width != nullptr && height != nullptr);

	constexpr unsigned int NUM_SIG_BYTES = 8;
	png_byte sig[NUM_SIG_BYTES];
	reader.Read(sig, NUM_SIG_BYTES);
	VERIFY(!png_sig_cmp(sig, 0, NUM_SIG_BYTES), "%s invalid png signature.", reader.Name());

	png_structp pngPtr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, PngError, PngWarning);
	VERIFY(pngPtr != nullptr, "%s cannot create png structure.", reader.Name());

	png_infop infoPtr = png_create_info_struct(pngPtr);
	if (infoPtr == nullptr) {
		png_destroy_read_struct(&pngPtr, nullptr, nullptr);
		VERIFY(false, "%s cannot create png info structure.", reader.Name());
	}

	if (setjmp(png_jmpbuf(pngPtr))) {
		png_destroy_read_struct(&pngPtr, &infoPtr, nullptr);
		VERIFY(false, "Cannot open %s.", reader.Name());
	}

	png_set_read_fn(pngPtr, &reader, PngRead);
	png_set_sig_bytes(pngPtr, NUM_SIG_BYTES);
	
	png_read_info(pngPtr, infoPtr);
	
	int bitDepth = 0;
	int colorType = 0;
	png_get_IHDR(pngPtr, infoPtr, width, height, &bitDepth, &colorType, nullptr, nullptr, nullptr);
	ASSERT(*width > 0 && *height > 0);
	ASSERT(bitDepth == 8);
	ASSERT(colorType == PNG_COLOR_TYPE_RGB_ALPHA);

#if 0
	constexpr double screenGamma = 2.2;  // A good guess for a PC monitor in a dimly lit room; 1.7 or 1.0 A good guess for Mac systems
	int intent;
	if (png_get_sRGB(pngPtr, infoPtr, &intent)) {
		png_set_gamma(pngPtr, screenGamma, 0.45455);
	}
	else {
		double imageGamma;
		if (png_get_gAMA(pngPtr, infoPtr, &imageGamma)) {
			png_set_gamma(pngPtr, screenGamma, imageGamma);
		}
		else {
			png_set_gamma(pngPtr, screenGamma, 0.45455);
		}
	}
	png_read_update_info(pngPtr, infoPtr);
#endif

	png_uint_32 numRowBytes = png_get_rowbytes(pngPtr, infoPtr);
	std::vector<png_bytep> rowPointers(*height);
	for (png_uint_32 row = 0; row < *height; ++row) {
		rowPointers[row] = static_cast<png_bytep>(buffer) + row * numRowBytes;
	}
	png_read_image(pngPtr, rowPointers.data());

	png_destroy_read_struct(&pngPtr, &infoPtr, nullptr);	
}

} /* namespace Core */
