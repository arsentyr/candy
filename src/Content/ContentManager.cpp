#include "Content/ContentManager.h"

#include "Assert.h"
#include "Content/ContentLoader.h"

namespace Core { namespace {

ContentManager content;

} /* anonymous namespace */

ContentManager& Content = content;

ContentManager::ContentManager() {
	CreateTextures();
	CreateAnimations();
	CreateSounds();
}

void ContentManager::Initialize() {
	Reload();
	for (auto& pair : _samples) {
		const auto& path = pair.first;
		auto& sample = pair.second;
		Loader.LoadSoundSample(sample, (path + ".ogg"));
	}
	Loader.LoadSoundStream(_stream, "music.ogg");
}

void ContentManager::Reload() {
	for (auto& pair : _textures) {
		const auto& path = pair.first;
		auto& texture = pair.second;
		texture.Lose();
		Loader.LoadTexture(texture, (path + ".png"));
	}
}

void ContentManager::Release() {
	for (auto& texture : _textures) {
		texture.second.Unload();
	}
	for (auto& sample : _samples) {
		sample.second.Unload();
	}
	_stream.Unload();
}

Texture& ContentManager::GetTexture(const std::string& textureId) {
	ASSERT(_textures.find(textureId) != _textures.end());
	return _textures.at(textureId);
}

AnimationContent& ContentManager::GetAnimation(const std::string& animationId) {
	ASSERT(_animations.find(animationId) != _animations.end());
	return _animations.at(animationId);
}

SoundSample& ContentManager::GetSoundSample(const std::string& sampleId) {
	ASSERT(_samples.find(sampleId) != _samples.end());
	return _samples.at(sampleId);
}

SoundStream& ContentManager::GetSoundStream() {
	return _stream;
}

void ContentManager::CreateTextures() {
	_textures.emplace("background", Texture());
	_textures.emplace("pedestal", Texture());
	_textures.emplace("snow_small", Texture());
	_textures.emplace("snow_medium", Texture());
	_textures.emplace("snow_large", Texture());
	_textures.emplace("progress_anim", Texture());
	_textures.emplace("reset", Texture());
	_textures.emplace("cell", Texture());
	_textures.emplace("cake", Texture());
	_textures.emplace("candy", Texture());
	_textures.emplace("donut", Texture());
	_textures.emplace("star_anim", Texture());
	_textures.emplace("star_disappear_anim", Texture());
	_textures.emplace("omnom_1_anim", Texture());
	_textures.emplace("omnom_2_anim", Texture());
	_textures.emplace("confetti_anim", Texture());
}

void ContentManager::CreateAnimations() {
	CreateProgressAnimation();
	CreateStarAnimation();
	CreateStarDisappearAnimation();
	CreateOmNomFirstAnimation();
	CreateOmNomSecondAnimation();
	CreateConfettiAnimation();
}

void ContentManager::CreateProgressAnimation() {
	constexpr int COUNT = 11;
	constexpr float DURATION = 1;
	const AnimationFrame frames[COUNT] = {
		{42, 42, 0.0159, 0.0020, 0.6721, 0.0840}, // 1
		{29, 42, 0.0159, 0.0881, 0.4690, 0.1701}, // 2
		{17, 42, 0.7143, 0.0020, 0.9799, 0.0840}, // 11
		{6, 42, 0.5079, 0.0881, 0.6017, 0.1701}, // 9
		{13, 42, 0.6349, 0.0881, 0.8380, 0.1701}, // 10
		{23, 42, 0.0159, 0.1742, 0.3752, 0.2562}, // 3
		{30, 42, 0.4127, 0.1742, 0.8814, 0.2562}, // 8
		{35, 42, 0.0159, 0.2603, 0.5627, 0.3423}, // 4
		{39, 42, 0.0159, 0.3464, 0.6252, 0.4284}, // 5
		{41, 42, 0.0159, 0.4325, 0.6565, 0.5145}, // 6
		{42, 42, 0.0159, 0.5186, 0.6721, 0.6006} // 7
	};
	_animations["progress"] = {
		"progress_anim",
		std::vector<AnimationFrame>(frames, (frames + COUNT)),
		{
			{"rotation", {0, COUNT, DURATION, TweenMode::Forward}}
		}
	};
}

void ContentManager::CreateStarAnimation() {
	constexpr int COUNT = 17;
	constexpr float DURATION = 1.25;
	const AnimationFrame frames[COUNT] = {
		{37, 37, 0.0709, 0.0196, 0.3599, 0.1641}, // 1
		{34, 37, 0.3937, 0.0196, 0.6593, 0.1641}, // 12
		{31, 37, 0.6929, 0.0196, 0.9351, 0.1641}, // 17
		{28, 37, 0.0709, 0.1804, 0.2896, 0.3249}, // 2
		{25, 37, 0.3228, 0.1804, 0.5181, 0.3249}, // 9
		{23, 37, 0.5512, 0.1804, 0.7309, 0.3249}, // 14
		{19, 37, 0.7638, 0.1804, 0.9122, 0.3249}, // 18
		{17, 37, 0.0709, 0.3412, 0.2037, 0.4857}, // 3
		{14, 37, 0.0709, 0.5020, 0.1802, 0.6504}, // 4
		{14, 37, 0.2126, 0.5020, 0.3220, 0.6504}, // 7
		{17, 37, 0.2362, 0.3412, 0.3690, 0.4857}, // 8
		{20, 37, 0.4016, 0.3412, 0.5578, 0.4857}, // 13
		{23, 37, 0.5906, 0.3412, 0.7702, 0.4857}, // 15
		{27, 37, 0.3543, 0.5020, 0.5653, 0.6465}, // 10
		{30, 37, 0.5984, 0.5020, 0.8328, 0.6465}, // 16
		{33, 37, 0.0709, 0.6667, 0.3287, 0.8112}, // 5
		{36, 37, 0.3622, 0.6667, 0.6435, 0.8112} // 11
	};
	_animations["star"] = {
		"star_anim",
		std::vector<AnimationFrame>(frames, (frames + COUNT)),
		{
			{"rotation", {0, COUNT, DURATION, TweenMode::LoopForward}}
		}
	};
}

void ContentManager::CreateStarDisappearAnimation() {
	constexpr int COUNT = 4;
	constexpr float DURATION = 0.75;
	const AnimationFrame frames[COUNT] = {
		{118, 126, 0.0020, 0.0000, 0.2324, 0.9844}, //1
		{98, 109, 0.2368, 0.0000, 0.4282, 0.8516}, //2
		{141, 125, 0.4325, 0.0000, 0.7079, 0.9766}, //3
		{115, 109, 0.7123, 0.0000, 0.9369, 0.8516} //4
	};
	_animations["star_disappear"] = {
		"star_disappear_anim",
		std::vector<AnimationFrame>(frames, (frames + COUNT)),
		{
			{"disappear", {0, (COUNT - 1), DURATION, TweenMode::Forward}}
		}
	};
}

void ContentManager::CreateOmNomFirstAnimation() {
	constexpr int EAT_COUNT = 9;
	constexpr float EAT_DURATION = 0.9;
	constexpr int CREW_COUNT = 9;
	constexpr float CREW_DURATION = 0.6;
	constexpr int COUNT = EAT_COUNT + CREW_COUNT;
	const AnimationFrame frames[COUNT] = {
		// eat
		{108, 83, 0.2072, 0.0005, 0.3127, 0.0410}, // 40
		{99, 94, 0.1085, 0.0005, 0.2052, 0.0464}, // 28
		{98, 105, 0.0958, 0.1988, 0.1915, 0.2501}, // 16
		{98, 101, 0.4057, 0.0967, 0.5014, 0.1460}, // 60
		{98, 98, 0.5044, 0.0479, 0.6001, 0.0957}, // 72
		{98, 97, 0.6022, 0.0479, 0.6979, 0.0952}, // 81
		{98, 97, 0.6999, 0.0479, 0.7956, 0.0952}, // 92
		{98, 97, 0.7977, 0.0479, 0.8934, 0.0952}, // 102
		{98, 97, 0.8954, 0.0479, 0.9911, 0.0952}, // 113
		// chew
		{99, 99, 0.6129, 0.0967, 0.7096, 0.1451}, // 83
		{110, 98, 0.5034, 0.0967, 0.6108, 0.1446}, // 71
		{122, 95, 0.3148, 0.0005, 0.4339, 0.0469}, // 51
		{118, 100, 0.7116, 0.0967, 0.8269, 0.1456}, // 94
		{108, 104, 0.4878, 0.1470, 0.5932, 0.1978}, // 69
		{99, 107, 0.1935, 0.2511, 0.2902, 0.3033}, // 32
		{98, 108, 0.1007, 0.3043, 0.1964, 0.3571}, // 26
		{98, 107, 0.0958, 0.2511, 0.1915, 0.3033}, // 17
		{98, 104, 0.3900, 0.1470, 0.4857, 0.1978} // 55
	};
	_animations["omnom_first"] = {
		"omnom_1_anim",
		std::vector<AnimationFrame>(frames, (frames + COUNT)),
		{
			{"eat",  {0, EAT_COUNT, EAT_DURATION, TweenMode::Forward}},
		 	{"crew", {EAT_COUNT, CREW_COUNT, CREW_DURATION, TweenMode::LoopForward}}
		}
	};
}

void ContentManager::CreateOmNomSecondAnimation() {
	constexpr int SITS_COUNT = 19;
	constexpr float SITS_DURATION = 2;
	constexpr int SHAKE_COUNT = 15;
	constexpr float SHAKE_DURATION = 2;
	constexpr int COUNT = SITS_COUNT + SHAKE_COUNT;
	const AnimationFrame frames[COUNT] = {
		// sits
		{98, 101, 0.0020, 0.0005, 0.1934, 0.0498}, // 1
		{98, 101, 0.1977, 0.0005, 0.3891, 0.0498}, // 15
		{98, 101, 0.3933, 0.0005, 0.5848, 0.0498}, // 28
		{98, 101, 0.5890, 0.0005, 0.7804, 0.0498}, // 40
		{98, 101, 0.7847, 0.0005, 0.9761, 0.0498}, // 52
		{98, 102, 0.0020, 0.0508, 0.1934, 0.1006}, // 2
		{98, 104, 0.0020, 0.1016, 0.1934, 0.1524}, // 3
		{98, 105, 0.0020, 0.1534, 0.1934, 0.2047}, // 4
		{98, 107, 0.0020, 0.2057, 0.1934, 0.2579}, // 5
		{98, 108, 0.0020, 0.2589, 0.1934, 0.3116}, // 6
		{98, 108, 0.1977, 0.2589, 0.3891, 0.3116}, // 20
		{98, 108, 0.3933, 0.2589, 0.5848, 0.3116}, // 32
		{98, 109, 0.0020, 0.3127, 0.1934, 0.3659}, // 7
		{98, 109, 0.1977, 0.3127, 0.3891, 0.3659}, // 21
		{98, 109, 0.3933, 0.3127, 0.5848, 0.3659}, // 33
		{98, 107, 0.1977, 0.2057, 0.3891, 0.2579}, // 19
		{98, 106, 0.3933, 0.2057, 0.5848, 0.2574}, // 31
		{98, 104, 0.1977, 0.1016, 0.3891, 0.1524}, // 17
		{98, 102, 0.1977, 0.0508, 0.3891, 0.1006}, // 16
		// shake
		{99, 94, 0.4149, 0.4714, 0.6082, 0.5173}, // 36
		{98, 101, 0.6125, 0.4714, 0.8039, 0.5207}, // 48
		{99, 104, 0.2114, 0.5247, 0.4031, 0.5750}, // 24
		{99, 106, 0.4090, 0.5247, 0.6008, 0.5760}, // 35
		{100, 105, 0.6067, 0.5247, 0.8004, 0.5755}, // 47
		{100, 104, 0.0020, 0.5789, 0.1957, 0.6292}, // 12
		{100, 99, 0.2016, 0.5789, 0.3953, 0.6268}, // 22
		{100, 94, 0.4012, 0.5789, 0.5949, 0.6243}, // 34
		{98, 95, 0.8082, 0.4714, 0.9980, 0.5173}, // 59
		{98, 100, 0.8063, 0.5247, 0.9961, 0.5730}, // 58
		{99, 103, 0.6008, 0.5789, 0.7926, 0.6287}, // 45
		{100, 104, 0.7984, 0.5789, 0.9922, 0.6292}, // 54
		{101, 104, 0.0020, 0.6307, 0.1977, 0.6810}, // 13
		{101, 105, 0.0020, 0.6825, 0.1977, 0.7333}, // 14
		{100, 100, 0.2035, 0.6307, 0.3973, 0.6790} // 23
	};
	_animations["omnom_second"] = {
		"omnom_2_anim",
		std::vector<AnimationFrame>(frames, (frames + COUNT)),
		{
			{"sits", {0, SITS_COUNT, SITS_DURATION, TweenMode::LoopForward}},
			{"shake", {SITS_COUNT, SHAKE_COUNT, SHAKE_DURATION, TweenMode::Forward}}
		}
	};
}

void ContentManager::CreateConfettiAnimation() {
	constexpr int GREEN_COUNT = 9;
	constexpr int RED_COUNT = 9;
	constexpr int BLUE_COUNT = 9;
	constexpr int COUNT = GREEN_COUNT + RED_COUNT + BLUE_COUNT;
	constexpr float DURATION = 1;
	const AnimationFrame frames[COUNT] = {
		// green
		{21, 19, 0.0323, 0.0010, 0.6885, 0.0195}, // 1
		{21, 19, 0.0323, 0.0215, 0.6885, 0.0401}, // 2
		{23, 17, 0.0323, 0.0420, 0.7510, 0.0586}, // 3
		{23, 17, 0.0323, 0.0606, 0.7510, 0.0772}, // 4
		{22, 19, 0.0323, 0.0792, 0.7198, 0.0977}, // 5
		{21, 19, 0.0323, 0.0997, 0.6885, 0.1183}, // 6
		{20, 19, 0.0323, 0.1202, 0.6573, 0.1388}, // 7
		{21, 21, 0.0323, 0.1408, 0.6885, 0.1613}, // 8
		{20, 19, 0.0323, 0.1632, 0.6573, 0.1818}, // 9
		// red
		{23, 17, 0.0323, 0.1838, 0.7510, 0.2004}, // 10
		{24, 16, 0.0323, 0.2023, 0.7823, 0.2180}, // 11
		{25, 18, 0.0323, 0.2199, 0.8135, 0.2375}, // 12
		{25, 22, 0.0323, 0.2395, 0.8135, 0.2610}, // 13
		{25, 26, 0.0323, 0.2630, 0.8135, 0.2883}, // 14
		{25, 20, 0.0323, 0.2903, 0.8135, 0.3099}, // 15
		{25, 14, 0.0323, 0.3118, 0.8135, 0.3255}, // 16
		{23, 16, 0.0323, 0.3275, 0.7510, 0.3431}, // 17
		{23, 17, 0.0323, 0.3451, 0.7510, 0.3617}, // 18
		// blue
		{27, 27, 0.0323, 0.3636, 0.8760, 0.3900}, // 19
		{21, 27, 0.0323, 0.3920, 0.6885, 0.4184}, // 20
		{17, 27, 0.0323, 0.4203, 0.5635, 0.4467}, // 21
		{12, 27, 0.0323, 0.4487, 0.4073, 0.4750}, // 22
		{6, 26, 0.7742, 0.3920, 0.9617, 0.4174}, // 27
		{6, 26, 0.6452, 0.4203, 0.8327, 0.4457}, // 26
		{12, 27, 0.4839, 0.4487, 0.8589, 0.4750}, // 25
		{17, 27, 0.0323, 0.4770, 0.5635, 0.5034}, // 23
		{21, 27, 0.0323, 0.5054, 0.6885, 0.5317} // 24
	};
	_animations["confetti"] = {
		"confetti_anim",
		std::vector<AnimationFrame>(frames, (frames + COUNT)),
		{
			{"green", {0, GREEN_COUNT, DURATION, TweenMode::LoopForward}},
			{"red", {GREEN_COUNT, RED_COUNT, DURATION, TweenMode::LoopForward}},
			{"blue", {(GREEN_COUNT + RED_COUNT), BLUE_COUNT, DURATION, TweenMode::LoopForward}}
		}
	};
}

void ContentManager::CreateSounds() {
	_samples.emplace("swap", SoundSample());
	_samples.emplace("ping_pong", SoundSample());
	_samples.emplace("reset", SoundSample());
	_samples.emplace("match", SoundSample());
	_samples.emplace("crew", SoundSample());
	_samples.emplace("eat", SoundSample());
	_samples.emplace("star", SoundSample());
}

} /* namespace Core */
