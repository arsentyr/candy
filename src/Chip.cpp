#include "Chip.h"

namespace Candy {

/* Chip */

Chip::Chip(bool isLocked)
		: _isLocked(isLocked) {
}

Chip::~Chip() {}

void Chip::SetPosition(float x, float y) {
	_x = x;
	_y = y;
}

void Chip::SetPosition(const Vector2& position) {
	SetPosition(position.x, position.y);
}

float Chip::Left() const {
	return _x;
}

float Chip::Right() const {
	return _x + Width();
}

float Chip::Bottom() const {
	return _y;
}

float Chip::Top() const {
	return _y + Height();
}

void Chip::Lock() {
	_isLocked = true;
}

void Chip::Unlock() {
	ASSERT(Type() != ChipType::Hidden);
	_isLocked = (Type() == ChipType::Hidden);
}

bool Chip::IsLocked() const {
	return _isLocked;
}

/* Hidden */

Hidden::Hidden()
		: Chip(true) {
}

void Hidden::Draw() const {
	ASSERT(false);
}

ChipType Hidden::Type() const {
	return ChipType::Hidden;
}

/* Cake */

Cake::Cake()
		: Chip(false)
		, _cake("cake") {
}

void Cake::SetPosition(float x, float y) {
	Chip::SetPosition(x, y);
	_cake.SetPosition(
		x + (Width() - _cake.Width()) / 2.f,
		y + (Height() - _cake.Height()) / 2.f
	);
}

void Cake::Draw() const {
	_cake.Draw();
}

ChipType Cake::Type() const {
	return ChipType::Cake;
}

/* Candy */

Candy::Candy()
		: Chip(false)
		, _candy("candy") {
}

void Candy::SetPosition(float x, float y) {
	Chip::SetPosition(x, y);
	_candy.SetPosition(
		x + (Width() - _candy.Width()) / 2.f,
		y + (Height() - _candy.Height()) / 2.f
	);
}

void Candy::Draw() const {
	_candy.Draw();
}

ChipType Candy::Type() const {
	return ChipType::Candy;
}

/* Donut */

Donut::Donut()
		: Chip(false)
		, _donut("donut") {
}

void Donut::SetPosition(float x, float y) {
	Chip::SetPosition(x, y);
	_donut.SetPosition(
		x + (Width() - _donut.Width()) / 2.f,
		y + (Height() - _donut.Height()) / 2.f
	);
}

void Donut::Draw() const {
	_donut.Draw();
}

ChipType Donut::Type() const {
	return ChipType::Donut;
}

/* Star */

Star::Star()
		: Chip(false)
		, _star("star")
		, _dy((-Chip::Height() / 12.f), (Chip::Height() / 12.f), 3, TweenEasing::QuadInOut, TweenMode::LoopPingPong, true)
		, _alpha(0.075, 0.325, 3, TweenEasing::QuadInOut, TweenMode::LoopPingPong, true) {
	_star.Play("rotation");
}

void Star::SetPosition(float x, float y) {
	Chip::SetPosition(x, y);
	_star.SetPosition(
		x + (Width() - _star.Width()) / 2.f,
		y + (Height() - _star.Height()) / 2.f
	);
}

void Star::Update(float deltaTime) {
	if (!IsLocked()) {
		_dy.Update(deltaTime);
	}
	_alpha.Update(deltaTime);
	_star.Update(deltaTime);
	_star.SetPosition(
		Left() + (Width() - _star.Width()) / 2.f,
		Bottom() + (Height() - _star.Height()) / 2.f + _dy
	);
}

void Star::Draw() const {
	_star.Draw();
	Gfx.SetMatrixMode(MatrixMode::Projection);
	Gfx.SetBlendMode(BlendMode::Add);
	Gfx.SetColor(0.5, 0.5, 0.5, _alpha);
	Gfx.PushMatrix();
	const auto dx = _star.Left() + _star.Width() / 2.f;
	const auto dy = _star.Bottom() + _star.Height() / 2.f;
	Gfx.Translate(dx, dy);
	Gfx.Scale(1.25, 1.25);
	Gfx.Translate(-dx, -dy);
	_star.Draw();
	Gfx.PopMatrix();
	Gfx.ResetColor();
	Gfx.ResetBlendMode();
	Gfx.ResetMatrixMode();
}

ChipType Star::Type() const {
	return ChipType::Star;
}

} /* namespace Candy */
