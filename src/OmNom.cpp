#include "OmNom.h"

namespace Candy {

OmNom::OmNom() {
	SetMode(OmNomMode::Sits);
	SetPosition(0, 0);
}

void OmNom::SetPosition(float x, float y) {
	_x = x;
	_y = y;
	_animation.SetPosition(
		_x - _animation.Width() / 2.f,
		_y
	);
}

float OmNom::Left() const {
	return _x;
}

float OmNom::Bottom() const {
	return _y;
}

void OmNom::SetMode(OmNomMode mode) {
	if (_animation.IsPlaying()) {
		_animation.Pause();
	}
	switch (mode) {
	case OmNomMode::Eat:
		if (_mode != OmNomMode::Eat) {
			_animation.SetContent("omnom_first");
			_sfxCrew.Stop();
			_sfxEat.Play(0.35);
		}
		_animation.Play("eat");
		break;
	case OmNomMode::Crew:
		_animation.SetContent("omnom_first");
		_animation.Play("crew");
		_duration = _animation.Duration() * 3;
		_sfxCrew.Stop();
		_sfxCrew.Play(0.35);
		break;
	case OmNomMode::Sits:
		_animation.SetContent("omnom_second");
		_animation.Play("sits");
		_duration = _animation.Duration() * 4;
		break;
	case OmNomMode::Shake:
		_animation.SetContent("omnom_second");
		_animation.Play("shake");
		break;
	}
	_mode = mode;
}

void OmNom::Update(float deltaTime) {
	_animation.Update(deltaTime);
	switch (_mode) {
	case OmNomMode::Eat:
		if (_animation.IsFinished()) {
			SetMode(OmNomMode::Crew);
		}
		break;
	case OmNomMode::Crew:
		_duration -= deltaTime;
		if (_duration <= 0) {
			SetMode(OmNomMode::Shake);
		}
		break;
	case OmNomMode::Sits:
		_duration -= deltaTime;
		if (_duration <= 0) {
			SetMode(OmNomMode::Shake);
		}
		break;
	case OmNomMode::Shake:
		if (_animation.IsFinished()) {
			SetMode(OmNomMode::Sits);
		}
		break;
	}
	_animation.SetPosition(
		_x - _animation.Width() / 2.f,
		_y
	);
}

void OmNom::Draw() {
	_animation.Draw();
}

} /* namespace Candy */
