#include "Effect.h"

namespace Candy {

/* SwapEffect */

SwapEffect::SwapEffect(Chip& first, Chip& second, float duration)
		: _first(first)
		, _second(second) {
	ASSERT(_first.IsLocked() && _second.IsLocked());
	_firstPosition.SetEquation(TweenEasing::QuadOut);
	_firstPosition.SetInitial(Vector2(_second.Left(), _second.Bottom()));
	_firstPosition.SetEnd(Vector2(_first.Left(), _first.Bottom()));
	_firstPosition.SetTime(0);
	_firstPosition.SetDuration(duration);
	_firstPosition.Resume();
	_secondPosition.SetEquation(TweenEasing::QuadOut);
	_secondPosition.SetInitial(_firstPosition.GetEnd());
	_secondPosition.SetEnd(_firstPosition.GetInitial());
	_secondPosition.SetTime(0);
	_secondPosition.SetDuration(duration);
	_secondPosition.Resume();
	Update(0);
}

SwapEffect::~SwapEffect() {
	if (!IsFinished()) {
		Finish();
	}
}

void SwapEffect::Update(float deltaTime) {
	ASSERT(!IsFinished() && _first.IsLocked() && _second.IsLocked());
	_firstPosition.Update(deltaTime);
	_secondPosition.Update(deltaTime);
	if (IsFinished()) {
		Finish();
	}
	else {
		_first.SetPosition(_firstPosition);
		_secondTrail.push_back(_secondPosition);
	}
}

void SwapEffect::Draw() {
	ASSERT(!IsFinished() && _first.IsLocked() && _second.IsLocked());
	auto count = 0;
	const auto alpha = _secondTrail.size() * 4.5f;
	for (const auto& position : _secondTrail) {
		++count;
		Gfx.SetColor(1, 1, 1, (count / alpha));
		_second.SetPosition(position);
		_second.Draw();
	}
	Gfx.ResetColor();
	_first.Draw();
	_second.Draw();
}

bool SwapEffect::IsFinished() const {
	return _firstPosition.IsFinished();
}

void SwapEffect::Finish() {
	_firstPosition.SetTime(_firstPosition.GetDuration());
	_firstPosition.Update(0);
	_secondPosition.SetTime(_secondPosition.GetDuration());
	_secondPosition.Update(0);
	_first.SetPosition(_firstPosition.GetEnd());
	_first.Unlock();
	_second.SetPosition(_secondPosition.GetEnd());
	_second.Unlock();
	_secondTrail.clear();
	ASSERT(IsFinished());
}

/* PingPongEffect */

PingPongEffect::PingPongEffect(Chip& first, Chip& second, float duration)
		: _first(first)
		, _second(second) {
	ASSERT(_first.IsLocked() && _second.IsLocked());
	_firstPosition.SetEquation(TweenEasing::QuadOut);
	_firstPosition.SetInitial(Vector2(_second.Left(), _second.Bottom()));
	_firstPosition.SetEnd(Vector2(_first.Left(), _first.Bottom()));
	_firstPosition.SetTime(0);
	_firstPosition.SetDuration(duration / 2.f);
	_firstPosition.Resume();
	_secondPosition.SetEquation(TweenEasing::QuadOut);
	_secondPosition.SetInitial(_firstPosition.GetEnd());
	_secondPosition.SetEnd(_firstPosition.GetInitial());
	_secondPosition.SetTime(0);
	_secondPosition.SetDuration(duration / 2.f);
	_secondPosition.Resume();
	Update(0);
}

PingPongEffect::~PingPongEffect() {
	if (!IsFinished()) {
		Finish();
	}
}

void PingPongEffect::Update(float deltaTime) {
	ASSERT(!IsFinished() && _first.IsLocked() && _second.IsLocked());
	_firstPosition.Update(deltaTime);
	_secondPosition.Update(deltaTime);
	if (IsFinished()) {
		if (_isForward) {
			_isForward = false;
			_firstPosition.SetInitial(_firstPosition.GetEnd());
			_firstPosition.SetEnd(_secondPosition.GetEnd());
			_firstPosition.SetTime(0);
			_firstPosition.Resume();
			_secondPosition.SetInitial(_firstPosition.GetEnd());
			_secondPosition.SetEnd(_firstPosition.GetInitial());
			_secondPosition.SetTime(0);
			_secondPosition.Resume();
		}
		else {
			Finish();
		}
	}
	else {
		_first.SetPosition(_firstPosition);
		_second.SetPosition(_secondPosition);
	}
}

void PingPongEffect::Draw() {
	ASSERT(!IsFinished() && _first.IsLocked() && _second.IsLocked());
	_first.Draw();
	_second.Draw();
}

bool PingPongEffect::IsFinished() const {
	return _firstPosition.IsFinished();
}

void PingPongEffect::Finish() {
	_firstPosition.SetTime(_firstPosition.GetDuration());
	_firstPosition.Update(0);
	_secondPosition.SetTime(_secondPosition.GetDuration());
	_secondPosition.Update(0);
	if (_isForward) {
		_first.SetPosition(_firstPosition.GetInitial());
		_second.SetPosition(_secondPosition.GetInitial());
	}
	else {
		_first.SetPosition(_firstPosition.GetEnd());
		_second.SetPosition(_secondPosition.GetEnd());
	}
	_first.Unlock();
	_second.Unlock();
	ASSERT(IsFinished());
}

/* DropEffect */

DropEffect::DropEffect(Chip& chip, float top, float bottom, float duration, float alpha, float delay)
		: _chip(chip)
		, _bottom(top, bottom, duration, EasingBack(1.15), TweenMode::Forward, true)
		, _alpha(alpha, 1, (duration / 4.f), TweenEasing::Linear, TweenMode::Forward, true)
		, _delay(delay) {
	ASSERT(_chip.IsLocked());
	Update(0);
}

DropEffect::~DropEffect() {
	if (!IsFinished()) {
		Finish();
	}
}

void DropEffect::Update(float deltaTime) {
	ASSERT(!IsFinished() && _chip.IsLocked());
	_delay -= deltaTime;
	if (_delay <= 0) {
		_bottom.Update(deltaTime);
		_alpha.Update(deltaTime);
		if (IsFinished()) {
			Finish();
		}
		else {
			_chip.SetPosition(_chip.Left(), _bottom);
		}
	}
}

void DropEffect::Draw() {
	ASSERT(!IsFinished() && _chip.IsLocked());
	if (_delay <= 0) {
		Gfx.SetColor(1, 1, 1, _alpha);
		_chip.Draw();
		Gfx.ResetColor();
	}
}

bool DropEffect::IsFinished() const {
	return _bottom.IsFinished();
}

void DropEffect::Finish() {
	_bottom.SetTime(_bottom.GetDuration());
	_bottom.Update(0);
	_chip.SetPosition(_chip.Left(), _bottom.GetEnd());
	_chip.Unlock();
	ASSERT(IsFinished());
}

/* RemoveEffect */

RemoveEffect::RemoveEffect(Chip& chip, float x0, float y0, float x1, float y1, float duration)
		: _chip(chip) {
	ASSERT(_chip.IsLocked());
	_x.SetInitial(x0);
	_x.SetEnd(x1);
	_x.SetDuration(duration);
	_y.SetInitial(y0);
	_y.SetEnd(y1);
	_y.SetDuration(duration);
	_alpha.SetDuration(duration);
	Update(0);
}

RemoveEffect::~RemoveEffect() {
	if (!IsFinished()) {
		Finish();
	}
}

void RemoveEffect::Update(float deltaTime) {
	ASSERT(!IsFinished() && _chip.IsLocked());
	_x.Update(deltaTime);
	_y.Update(deltaTime);
	_alpha.Update(deltaTime);
	if (IsFinished()) {
		Finish();
	}
	_chip.SetPosition(_x, _y);
}

void RemoveEffect::Draw() {
	ASSERT(!IsFinished() && _chip.IsLocked());
	Gfx.SetColor(1, 1, 1, _alpha);
	_chip.Draw();
	Gfx.ResetColor();
}

bool RemoveEffect::IsFinished() const {
	return _x.IsFinished();
}

void RemoveEffect::Finish() {
	_x.SetTime(_x.GetDuration());
	_x.Update(0);
	_y.SetTime(_y.GetDuration());
	_y.Update(0);	
	_chip.Unlock();
	ASSERT(IsFinished());
}

/* DisappearEffect */

DisappearEffect::DisappearEffect(float x, float y)
		: _x(x)
		, _y(y) {
	_disappear.Play("disappear");
	Update(0);
}

void DisappearEffect::Update(float deltaTime) {
	ASSERT(!IsFinished());
	_disappear.Update(deltaTime);
	_disappear.SetPosition(
		_x + (Chip::Width() - _disappear.Width()) / 2.f,
		_y + (Chip::Height() - _disappear.Height()) / 2.f
	);
}

void DisappearEffect::Draw() {
	ASSERT(!IsFinished());
	_disappear.Draw();
}

bool DisappearEffect::IsFinished() const {
	return _disappear.IsFinished();
}

/* Confetti Effect */

ConfettiEffect::ConfettiEffect(float duration) {
	_alpha.SetDuration(duration);
	auto count = 0;
	for (auto& anim : _animations) {
		switch (count) {
		case 0:
			anim.Play("green");
			++count;
			break;
		case 1:
			anim.Play("red");
			++count;
			break;
		case 2:
			anim.Play("blue");
			count = 0;
			break;
		}
		anim.Update(Rand.NextFloat(0, anim.Duration()));
	}
	for (auto& particle : _particles) {
		particle.alpha = Rand.NextFloat(0.65, 1);
		particle.velocity = Vector2(Rand.NextFloat(-0.4, 0.4), -1).Normalized() * Rand.NextFloat((LOGICAL_HEIGHT * 0.05), (LOGICAL_HEIGHT * 0.175));
		particle.position = Vector2(Rand.NextInt((LOGICAL_WIDTH * 0.075), (LOGICAL_WIDTH * 0.925)), Rand.NextInt((LOGICAL_HEIGHT * 0.85), (LOGICAL_HEIGHT * 1.05)));
	}
	Update(0);
}

void ConfettiEffect::Update(float deltaTime) {
	ASSERT(!IsFinished());
	for (auto& anim : _animations) {
		anim.Update(deltaTime);
	}
	for (auto& particle : _particles) {
		const auto deltaVelocity = particle.velocity * 0.1 * deltaTime;
		particle.velocity -= deltaVelocity;
		particle.position += particle.velocity * deltaTime;
	}
	_alpha.Update(deltaTime);
}

void ConfettiEffect::Draw() {
	ASSERT(!IsFinished());
	auto i = 0;
	for (auto& particle : _particles) {
		Gfx.SetColor(1, 1, 1, _alpha * particle.alpha);
		_animations[i].SetPosition(particle.position.x, particle.position.y);
		_animations[i].Draw();
		++i;
		if (i >= NUM_ANIMATIONS) {
			i = 0;
		}
	}
	Gfx.ResetColor();
}

bool ConfettiEffect::IsFinished() const {
	return _alpha.IsFinished();
}

} /* namespace Candy */
