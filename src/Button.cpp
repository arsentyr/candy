#include "Button.h"

namespace Candy {

Button::Button(float x, float y, const char* name, const ClickCallback& callback)
		: _x(x)
		, _y(y)
		, _texture(Content.GetTexture(name))
		, _callback(callback) {
	ASSERT(_callback);
}

void Button::SetPosition(float x, float y) {
	_x = x;
	_y = y;
}

int Button::Width() const {
	return _texture.Width();
}

int Button::Height() const {
	return _texture.Height();
}

void Button::Draw() {
	if (_hasTouch) {
		_texture.Draw((_x + Width() * 0.05), (_y - Height() * 0.05));
	}
	else {
		_texture.Draw(_x, _y);
	}
}

bool Button::TouchDown(float x, float y) {
	_hasTouch = Contains(x, y);
	return _hasTouch;
}

bool Button::TouchUp(float x, float y) {
	if (_hasTouch) {
		_hasTouch = false;
		if (Contains(x, y)) {
			_callback();
			return true;
		}
	}
	return false;
}

bool Button::TouchMove(float x, float y) {
	return _hasTouch && Contains(x, y);
}

inline bool Button::Contains(float x, float y) const {
	return x >= _x && x <= (_x + _texture.Width()) && y >= _y && y <= (_y + _texture.Height());
}

} /* namespace Candy */
