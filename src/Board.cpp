#include "Board.h"

#include <algorithm>

namespace Candy {

Board::Board(int rows, int cols, const ProgressCallback& callback)
		: _rows(rows)
		, _cols(cols)
		, _rect(
			(LOGICAL_WIDTH - cols * Chip::Width()) / 2.f,
			(LOGICAL_HEIGHT - rows * Chip::Height()) / 2.f + Chip::Height() * 0.7f,
			cols * Chip::Width(),
			rows * Chip::Height())
		, _callback(callback)
		, _cells(rows) {
	ASSERT(_rows >= 5 && cols >= 4);
	ASSERT(_callback);
	_pool.reserve(_rows * _cols);
	for (auto row = 0, y = _rect.y; row < _rows; ++row, y += Chip::Height()) {
		_cells[row].reserve(cols);
		for (auto col = 0, x = _rect.x; col < _cols; ++col, x += Chip::Width()) {
			switch (Rand.NextInt(0, 2)) {
			case 0:
				_cells[row].emplace_back(new Cake(), x, y);
				_pool.emplace_back(new Candy());
				break;
			case 1:
				_cells[row].emplace_back(new Candy(), x, y);
				_pool.emplace_back(new Donut());
				break;
			case 2:
				_cells[row].emplace_back(new Donut(), x, y);
				_pool.emplace_back(new Cake());
				break;
			}
		}
	}

	_cells[0][0].Reset(new Hidden());
	_cells[0][cols-1].Reset(new Hidden());
	_cells[_rows-1][0].Reset(new Hidden());
	_cells[_rows-1][_cols-1].Reset(new Hidden());
	for (auto count = static_cast<int>(((_rows - 4) * (_cols - 2)) / 3.f + 1); count > 0;) {
		const auto row = Rand.NextInt(2, (_rows - 3));
		const auto col = Rand.NextInt(1, (_cols - 2));
		auto& cell = _cells[row][col];
		if (cell.IsMatchable()) {
			cell.Reset(new Hidden(), cell->Left(), cell->Bottom());
			--count;
		}
	}

	for (auto& star : _stars) {
		star.Reset(new Star());
	}
}

void Board::Reset() {
	// delete effects
	_effects.clear();
	// delete stars from board
	for (auto& cols : _cells) {
		for (auto& cell : cols) {
			if (_numStars == 0) {
				break;
			}
			if (cell->Type() == ChipType::Star) {
				--_numStars;
				cell.To(_stars[_numStars]);
			}
		}
	}
	ASSERT(_numStars == 0);
	// reset cells without stars
	Refresh();
	for (auto row = 1; row < (_rows - 1); ++row) {
		for (auto col = 1; col < (_cols - 1); ++col) {
			auto& hidden = _cells[row][col];
			if (hidden->Type() == ChipType::Hidden) {
				hidden.SetPosition(hidden->Left(), hidden->Bottom());
				const auto r = Rand.NextInt(2, (_rows - 3));
				const auto c = Rand.NextInt(1, (_cols - 2));
				auto& machable = _cells[r][c];
				if (machable.IsMatchable()) {
					hidden.Swap(machable);
				}
			}
		}
	}
	// add stars to board
	do {
		const auto row = Rand.NextInt(2, (_rows - 1));
		const auto col = Rand.NextInt(0, (_cols - 1));
		auto& cell = _cells[row][col];
		if (cell.IsMatchable()) {
			cell.To(_stars[_numStars]);
			cell->Update(Rand.NextFloat(0, 1));
			++_numStars;
		}
	} while (_numStars < MAX_STARS);
	ASSERT(_numStars == MAX_STARS);
	ASSERT(all_of(begin(_cells), end(_cells), [](const std::vector<Cell>& cols){ return all_of(begin(cols), end(cols), [](const Cell& cell){ return !cell->IsLocked() || cell->Type() == ChipType::Hidden; }); }));
	ASSERT(all_of(begin(_pool), end(_pool), [](const Cell& cell){ return cell.IsMatchable() && !cell->IsLocked(); }));
	ASSERT(all_of(begin(_stars), end(_stars), [](const Cell& cell){ return cell.IsMatchable() && !cell->IsLocked(); }));
	// reset pan
	_panRow = -1;
	_panCol = -1;
	_panStarted = false;
	// reset OmNom
	_omNom.SetPosition((LOGICAL_WIDTH * 0.5f), (LOGICAL_HEIGHT * 0.05f));
	_omNom.SetMode(OmNomMode::Sits);
	// add drop effects
	for (auto row = 0; row < _rows; ++row) {
		for (auto col = 0; col < _cols; ++col) {
			auto& cell = _cells[row][col];
			if (cell->Type() != ChipType::Hidden) {
				cell.SetPosition(cell->Left(), cell->Bottom());
				cell->Lock();
				_effects.emplace_front(new DropEffect(
					*cell,
					LOGICAL_HEIGHT + row * Chip::Height(),
					cell->Bottom(),
					DROP_DURATION,
					0.5,
					col * DROP_DURATION * 0.2
				));
			}
		}
	}
	_sfxReset.Play(0.5);
}

bool Board::TouchDown(float x, float y) {
	return TouchMove(x, y);
}

bool Board::TouchUp(float x, float y) {
	if (_panStarted && TouchMove(x, y)) {
		_panStarted = false;
		return true;
	}
	return false;
}

bool Board::TouchMove(float x, float y) {
	if (x >= _rect.x && x < _rect.Right() && y >= _rect.y && y < _rect.Top()) {
		if (_panStarted) {
			const auto& cell = _cells[_panRow][_panCol];
			ASSERT(!cell->IsLocked());
			const auto row = (y < cell->Bottom())
				? (_panRow - 1)
				: (y > cell->Top()) ? (_panRow + 1) : _panRow;
			const auto col = (x < cell->Left())
				? (_panCol - 1)
				: (x > cell->Right()) ? (_panCol + 1) : _panCol;
			ASSERT(row >= 0 && row < _rows);
			ASSERT(col >= 0 && col < _cols);
			if (row == _panRow && col == _panCol) {
				return true;
			}
			if (((row == _panRow && col != _panCol) || (row != _panRow && col == _panCol))
					&& !_cells[row][col]->IsLocked()) {
				Swap(row, col);
			}
			_panStarted = false;
		}
		else {
			const auto row = static_cast<int>((y - _rect.y) / Chip::Height());
			const auto col = static_cast<int>((x - _rect.x) / Chip::Width());
			ASSERT(row >= 0 && row < _rows);
			ASSERT(col >= 0 && col < _cols);
			_panStarted = !_cells[row][col]->IsLocked();
			if (_panStarted) {
				_panRow = row;
				_panCol = col;
			}
		}
		return true;
	}
	else if (_panStarted) {
		_panStarted = false;
	}
	return false;
}

void Board::Update(float deltaTime) {
	_omNom.Update(deltaTime);
	for (const auto& cols : _cells) {
		for (const auto& cell : cols) {
			cell->Update(deltaTime);
		}
	}
	if (!_effects.empty()) {
		for (const auto& effect : _effects) {
			effect->Update(deltaTime);
		}
		_effects.remove_if([](const std::unique_ptr<Effect>& effect) { return effect->IsFinished(); });
		if (_effects.empty()) {
			if (_numStars == 0) {
				_callback(true);
				return;
			}
			Match();
			if (_numStars == 0) {
				_effects.emplace_back(new ConfettiEffect(CONFETTI_DURATION));
			}
		}
	}
	SetTouchable(_effects.empty());
}

void Board::Draw() {
	_omNom.Draw();
	for (const auto& cols : _cells) {
		for (const auto& cell : cols) {
			cell.Draw();
		}
	}
	for (const auto& effect : _effects) {
		effect->Draw();
	}
}

void Board::Refresh(Cell& cell) {
	ASSERT(cell.IsMatchable());
	while (true) {
		auto& pool = _pool[Rand.NextInt(0, (_rows * _cols - 1))];
		if (!pool->IsLocked()) {
			cell.To(pool);
			break;
		}
	}
}

void Board::Refresh() {
	for (auto row = 0; row < _rows; ++row) {
		for (auto col = 0; col < _cols; ++col) {
			auto& cell = _cells[row][col];
			if (cell.IsMatchable()) {
				auto count = 5;
				do {
					Refresh(cell);
				} while (HasMatch(row, col) && count-- > 0);
			}
		}
	}
}

void Board::Match() {
	bool hasMatches = false;
	for (auto row = 0; row < _rows; ++row) {
		for (auto col = 2; col < _cols; ++col) {
			if (_cells[row][col].IsMatchable()
					&& _cells[row][col]->Type() == _cells[row][col-1]->Type()
					&& _cells[row][col-1]->Type() == _cells[row][col-2]->Type()) {
				_cells[row][col]->Lock();
				_cells[row][col-1]->Lock();
				_cells[row][col-2]->Lock();
				if (!hasMatches) {
					_sfxMatch.Play(0.6);
					hasMatches = true;
				}
			}
		}
	}
	auto hasFinished = false;
	for (auto col = 0; col < _cols; ++col) {
		for (auto row = 2; row < _rows; ++row) {
			if (_cells[row][col].IsMatchable()
					&& _cells[row][col]->Type() == _cells[row-1][col]->Type()
					&& _cells[row-1][col]->Type() == _cells[row-2][col]->Type()) {
				_cells[row][col]->Lock();
				_cells[row-1][col]->Lock();
				_cells[row-2][col]->Lock();
				if (!hasMatches) {
					_sfxMatch.Play(0.6);
					hasMatches = true;
				}
			}
		}
		if (_cells[0][col]->Type() == ChipType::Star) {
			_cells[0][col]->Lock();
			hasFinished = true;
		}
	}
	if (hasMatches) {
		_omNom.SetMode(OmNomMode::Eat);
	}
	else if (!hasFinished) {
		return;
	}
	for (auto col = 0; col < _cols; ++col) {
		auto height = 0;
		for (auto row = 0; row < _rows; ++row) {
			if (_cells[row][col]->IsLocked()) {
				auto& cell = _cells[row][col];
				if (cell->Type() == ChipType::Star) {
					++height;
					--_numStars;
					_sfxStar.Play(0.85);
					cell->Unlock();
					cell.To(_stars[_numStars]);
					cell->Lock();
					_effects.emplace_back(new DisappearEffect(
						cell->Left(),
						cell->Bottom()
					));
					_callback(false);
					ASSERT(cell.IsMatchable());
				}
				else if (cell->Type() != ChipType::Hidden) {
					++height;
					_effects.emplace_back(new RemoveEffect(
						*cell,
						cell->Left(),
						cell->Bottom(),
						_omNom.Left() - Chip::Width() / 2.f,
						_omNom.Bottom() + 45 - Chip::Height() / 2.f,
						REMOVE_DURATION
					));
					Refresh(cell);
					cell->Lock();
					ASSERT(cell.IsMatchable());
				}
			}
			else if (height > 0) {
				auto top = row;
				for (auto bottom = (top - 1), count = height; count > 0; --bottom) {
					if (_cells[bottom][col]->Type() != ChipType::Hidden) {
						ASSERT(_cells[bottom][col]->IsLocked());
						_cells[top][col].Swap(_cells[bottom][col]);
						top = bottom;
						--count;
					}
				}
				if (top != row) {
					auto& cell = _cells[top][col];
					cell->Lock();
					_effects.emplace_front(new DropEffect(
						*cell,
						_cells[row][col]->Bottom(),
						cell->Bottom(),
						DROP_DURATION
					));
				}
			}
		}
		if (height > 0) {
			for (auto row = (_rows - 1); height > 0; --row) {
				auto& cell = _cells[row][col];
				if (cell->Type() != ChipType::Hidden) {
					ASSERT(cell->IsLocked() && cell->Type() != ChipType::Star);
					--height;
					_effects.emplace_front(new DropEffect(
						*cell,
						_rect.Top() + height * Chip::Height(),
						cell->Bottom(),
						DROP_DURATION,
						0.5
					));
				}
			}
		}
	}
}

bool Board::HasMatch(int row, int col) const {
	ASSERT(row >= 0 && row < _rows);
	ASSERT(col >= 0 && col < _cols);
	ASSERT(!_cells[row][col]->IsLocked());
	if (!_cells[row][col].IsMatchable()) {
		return false;
	}

	const auto type = _cells[row][col]->Type();
	const auto minCol = (col > 0 && _cells[row][col-1]->Type() == type)
		? (col > 1 && _cells[row][col-2]->Type() == type)
			? (col - 2)
			: (col - 1)
		: col;
	const auto maxCol = ((col + 1) < _cols && _cells[row][col+1]->Type() == type)
		? ((col + 2) < _cols && _cells[row][col+2]->Type() == type)
			? (col + 2)
			: (col + 1)
		: col;
	ASSERT(minCol >= 0 && maxCol < _cols && minCol <= maxCol);
	if ((maxCol - minCol) >= 2) {
		return true;
	}

	const auto minRow = (row > 0 && _cells[row-1][col]->Type() == type)
		? (row > 1 && _cells[row-2][col]->Type() == type)
			? (row - 2)
			: (row - 1)
		: row;
	const auto maxRow = ((row + 1) < _rows && _cells[row+1][col]->Type() == type)
		? ((row + 2) < _rows && _cells[row+2][col]->Type() == type)
			? (row + 2)
			: (row + 1)
		: row;
	ASSERT(minRow >= 0 && maxRow < _rows && minRow <= maxRow);
	return (maxRow - minRow) >= 2;
}

void Board::Swap(int row, int col) {
	ASSERT(_panStarted);
	auto& first = _cells[_panRow][_panCol];
	auto& second = _cells[row][col];
	ASSERT(!first->IsLocked() && !second->IsLocked());
	first.Swap(second);
	if (HasMatch(_panRow, _panCol) || HasMatch(row, col)) {
		_sfxSwap.Play(0.8);
		first->Lock();
		second->Lock();
		_effects.emplace_front(new SwapEffect(
			*first,
			*second,
			SWAP_DURATION
		));
	}
	else {
		_sfxPingPong.Play(0.8);
		first->Lock();
		second->Lock();
		_effects.emplace_front(new PingPongEffect(
			*first,
			*second,
			SWAP_DURATION
		));
		first.Swap(second);
	}
}

} /* namespace Candy */
