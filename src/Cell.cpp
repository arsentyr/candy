#include "Cell.h"

namespace Candy {

Cell::Cell(Chip* chip, float x, float y)
		: std::unique_ptr<Chip>(chip) {
	if (chip != nullptr) {
		SetPosition(x, y);
	}
}

void Cell::SetPosition(float x, float y) {
	ASSERT(get() != nullptr);
	_cell.SetPosition(
		x + (Chip::Width() - _cell.Width()) / 2.f,
		y + (Chip::Height() - _cell.Height()) / 2.f
	);
	get()->SetPosition(x, y);
}

void Cell::Reset(Chip* chip, float x, float y) {
	ASSERT(chip != nullptr && chip != get());
	reset(chip);
	SetPosition(x, y);
}

void Cell::To(Cell& cell) {
	ASSERT(this != &cell && get() != nullptr && cell.get() != nullptr);
	cell->SetPosition(get()->Left(), get()->Bottom());
	swap(cell);
}

void Cell::Swap(Cell& cell) {
	ASSERT(this != &cell && get() != nullptr && cell.get() != nullptr);
	const auto x = cell->Left();
	const auto y = cell->Bottom();
	cell->SetPosition(get()->Left(), get()->Bottom());
	get()->SetPosition(x, y);
	swap(cell);
}

void Cell::Draw() const {
	ASSERT(get() != nullptr);
	if (get()->Type() != ChipType::Hidden) {
		_cell.Draw();
	}
	if (!get()->IsLocked()) {
		get()->Draw();
	}
}

bool Cell::IsMatchable() const {
	ASSERT(get() != nullptr);
	return get()->Type() != ChipType::Hidden && get()->Type() != ChipType::Star; 
}

} /* namespace Candy */
