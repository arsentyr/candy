#include "Audio/SoundSample.h"

#include "Assert.h"

namespace Core {

SoundSample::~SoundSample() {
	ASSERT(!IsLoaded());
	if (IsLoaded()) {
		Unload();
	}
}

void SoundSample::Load(const void* data, int length) {
	ASSERT(data != nullptr && length > 0);
	ASSERT(!IsLoaded());
	if (IsLoaded()) {
		Unload();
	}
	_sample = BASS_SampleLoad(TRUE, data, 0, length, 65535, BASS_SAMPLE_OVER_POS);
	CHECK_BASS();
	ASSERT(IsLoaded());
}

void SoundSample::Unload() {
	BASS_SampleStop(_sample);
	BASS_SampleFree(_sample);
	CHECK_BASS();
	_sample = UNKNOWN_SAMPLE;
}

bool SoundSample::IsLoaded() const {
	return _sample != UNKNOWN_SAMPLE;
}

void SoundSample::Play(float volume) {
	ASSERT(volume >= 0 && volume <= 1);
	const auto channel = BASS_SampleGetChannel(_sample, TRUE);
	BASS_ChannelSetAttribute(channel, BASS_ATTRIB_VOL, volume);
	BASS_ChannelPlay(channel, TRUE);
	CHECK_BASS();
}

void SoundSample::Stop() {
	BASS_SampleStop(_sample);
	CHECK_BASS();
}

} /* namespace Core */
