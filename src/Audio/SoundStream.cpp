#include "Audio/SoundStream.h"

#include "Assert.h"

namespace Core { namespace {

void CALLBACK FileCloseProc(void* /*user*/) {}

QWORD CALLBACK FileLenProc(void* user) {
    return static_cast<DataReader*>(user)->Length();
}

DWORD CALLBACK FileReadProc(void* buffer, DWORD length, void* user) {
    return static_cast<DataReader*>(user)->Read(buffer, length);
}

} /* anonymous namespace */

SoundStream::~SoundStream() {
	ASSERT(!IsLoaded());
	if (IsLoaded()) {
		Unload();
	}
}

void SoundStream::Load(DataReader& reader) {
	ASSERT(reader.IsOpen());
	ASSERT(!IsLoaded());
	if (IsLoaded()) {
		Unload();
	}
	const BASS_FILEPROCS procs = {FileCloseProc, FileLenProc, FileReadProc, nullptr};
	_stream = BASS_StreamCreateFileUser(STREAMFILE_BUFFER, BASS_SAMPLE_LOOP, &procs, &reader);
	CHECK_BASS();
	ASSERT(IsLoaded());
}

void SoundStream::Unload() {
	BASS_StreamFree(_stream);
	CHECK_BASS();
	_stream = UNKNOWN_STREAM;
}

bool SoundStream::IsLoaded() const {
	return _stream != UNKNOWN_STREAM;
}

void SoundStream::Play(float volume) {
	ASSERT(volume >= 0 && volume <= 1);
	BASS_ChannelSetAttribute(_stream, BASS_ATTRIB_VOL, volume);
	BASS_ChannelPlay(_stream, FALSE);
	CHECK_BASS();
}

} /* namespace Core */
