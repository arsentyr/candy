#include "Audio/Sound.h"

#include "Audio/BASS.h"

namespace Core { namespace {

Sound sound;

} /* anonymous namespace */

Sound& Sfx = sound;

Sound::~Sound() {
	BASS_Free();
}

void Sound::Initialize() {
	BASS_Init(1, 44100, (BASS_DEVICE_MONO | BASS_DEVICE_LATENCY), 0, nullptr);
	BASS_SetConfig(BASS_CONFIG_UPDATETHREADS, 1);
	CHECK_BASS();
}

void Sound::Resume() {
	BASS_Start();
	CHECK_BASS();
}

void Sound::Pause() {
	BASS_Pause();
	CHECK_BASS();
}

void Sound::Release() {
	BASS_Stop();
	BASS_Free();
	CHECK_BASS();
}

} /* namespace Core */
