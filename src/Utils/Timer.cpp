#include "Utils/Timer.h"

#include "Assert.h"

namespace Core {

void Timer::Start() {
	ASSERT(!_started);
	_time = std::chrono::steady_clock::now();
	_started = true;
}

void Timer::Stop() {
	ASSERT(_started);
	_started = false;
}

float Timer::GetDeltaTime() {
	ASSERT(_started);
	if (!_started) {
		return 0;
	}

	const auto now = std::chrono::steady_clock::now();
	const auto deltaTime = std::chrono::duration<float>(now - _time).count();
	_time = now;
	return deltaTime;
}

} /* namespace Core */
