#include "Utils/Random.h"

#include "Assert.h"

namespace Core {

bool Random::NextBool() {
	return NextFloat(0, 10) < 5;
}

int Random::NextInt(int min, int max) {
	ASSERT(min <= max);
	return _intDistribution(_generator, IntDistribution::param_type(min, max));
}

float Random::NextFloat(float min, float max) {
	ASSERT(min <= max);
	return _floatDistribution(_generator, FloatDistribution::param_type(min, max));
}

} /* namespace Core */
