#include "Animation/TweenEquation.h"

namespace Core { namespace {

/* EasingLinear */

float EaseLinear(float t) {
	ASSERT(t >= 0 && t <= 1);
	return t;
}

/* EasingQuad */

float EaseInQuad(float t) {
	ASSERT(t >= 0 && t <= 1);
	return t * t;
}

float EaseOutQuad(float t) {
	ASSERT(t >= 0 && t <= 1);
	return -t * (t - 2);
}

float EaseInOutQuad(float t) {
	ASSERT(t >= 0 && t <= 1);
	t += t;
	if (t < 1) {
		return t * t / 2;
	}
	else {
		--t;
		return -0.5 * (t * (t - 2) - 1);
	}
}

/* EasingQuint */

float EaseInQuint(float t) {
	ASSERT(t >= 0 && t <= 1);
	return t * t * t * t * t;
}

} /* anonymous namespace */

TweenEquation::TweenEquation(TweenEasing easing) {
	switch (easing) {
	default: ASSERT(false); // through
	case TweenEasing::Linear: _equation = &EaseLinear; break;
	case TweenEasing::QuadIn: _equation = &EaseInQuad; break;
	case TweenEasing::QuadOut: _equation = &EaseOutQuad; break;
	case TweenEasing::QuadInOut: _equation = &EaseInOutQuad; break;
	case TweenEasing::QuintIn: _equation = &EaseInQuint; break;
	case TweenEasing::BackOut: _equation = EasingBack(); break;
	}
}

TweenEquation::TweenEquation(EasingBack&& easingBack) :
	_equation(easingBack) {
}

} /* namespace Core */
