#include "Animation/Animation.h"

#include "Assert.h"
#include "Content/ContentManager.h"
#include "Math/Math.h"

namespace Core {

Animation::Animation(const std::string& content, float x, float y)
		: _x(x)
		, _y(y)
		, _index(0)
		, _tween(0, 0, 1, TweenEasing::Linear)
		, _content(&Content.GetAnimation(content))
		, _texture(&Content.GetTexture(_content->name)) {
}

void Animation::SetContent(const std::string& content) {
	ASSERT(!IsPlaying());
	_content = &Content.GetAnimation(content);
	_texture = &Content.GetTexture(_content->name);
	_tween.SetInitial(0);
	_tween.SetEnd(_tween.GetInitial());
	_tween.SetDuration(1);
	_tween.SetTime(_tween.GetDuration());
	_tween.SetMode(TweenMode::Forward);
	_tween.Update(0);
}

void Animation::SetPosition(float x, float y) {
	_x = x;
	_y = y;
}

float Animation::Left() const {
	return _x;
}

float Animation::Right() const {
	return _x + _content->frames[_index].width;
}

float Animation::Bottom() const {
	return _y;
}

int Animation::Width() const {
	return _content->frames[_index].width;
}

int Animation::Height() const {
	return _content->frames[_index].height;
}

float Animation::Duration() const {
	return _tween.GetDuration();
}

void Animation::Play(const std::string& action) {
	ASSERT(_content->actions.count(action) > 0);
	const auto& current = _content->actions.at(action);
	ASSERT(current.initial < _content->frames.size() && (current.initial + current.count) <= _content->frames.size() && current.count > 0);
	_tween.SetInitial(current.initial);
	_tween.SetEnd(current.initial + current.count);
	_tween.SetTime(0);
	_tween.SetDuration(current.duration);
	_tween.SetMode(current.mode);
	_tween.Resume();
	Update(0);
}

void Animation::Pause() {
	_tween.Pause();
}
 
void Animation::Resume() {
	_tween.Resume();
}

bool Animation::Update(float deltaTime) {
	_tween.Update(deltaTime);
	if (_tween.IsTweening()) {
		_index = _tween.Value();
		ASSERT(_index < _content->frames.size());
		return true;
	}
	return false;
}

void Animation::Draw() const {
	const auto& frame = _content->frames[_index];
	_texture->Draw(_x, _y, 0, frame.width, frame.height, frame.u0, frame.v0, frame.u1, frame.v1);
}

bool Animation::IsPlaying() const {
	return _tween.IsTweening();
}

bool Animation::IsFinished() const {
	return _tween.IsFinished();
}

} /* namespace Core */
