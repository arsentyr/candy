#include "Progress.h"

#include "Assert.h"

namespace Candy {

void Progress::SetPosition(float x, float y) {
	_left.SetPosition(x, y);
	_middle.SetPosition((_left.Right() + LOGICAL_WIDTH * 0.01), y);
	_right.SetPosition((_middle.Right()  + LOGICAL_WIDTH * 0.01), y);
	ASSERT(_left.Left() < _middle.Left() && _middle.Left() < _right.Left());
}

void Progress::SetState(ProgressState state) {
	_state = state;
	_left.Play("rotation");
	_middle.Play("rotation");
	_right.Play("rotation");
	switch (_state) {
	case ProgressState::None:
		_left.Pause();
		_middle.Pause();
		_right.Pause();
		break;
	case ProgressState::Low:
		_middle.Pause();
		_right.Pause();
		break;
	case ProgressState::Medium:
		_left.Update(_left.Duration() * 0.95);
		_right.Pause();
		break;
	case ProgressState::High:
		_left.Update(_left.Duration() * 0.95);
		_middle.Update(_middle.Duration() * 0.95);
		break;
	}
	SetPosition(_left.Left(), _left.Bottom());
}

void Progress::Increase() {
	switch (_state) {
	case ProgressState::None: SetState(ProgressState::Low); break;
	case ProgressState::Low: SetState(ProgressState::Medium); break;
	case ProgressState::Medium: SetState(ProgressState::High); break;
	case ProgressState::High: ASSERT(false); break;
	}
}

int Progress::Width() const {
	return _right.Right() - _left.Left();
}

int Progress::Height() const {
	return _left.Height();
}

void Progress::Update(float deltaTime) {
	_left.Update(deltaTime);
	_middle.Update(deltaTime);
	_right.Update(deltaTime);
}

void Progress::Draw() {
	_left.Draw();
	_middle.Draw();
	_right.Draw();
}

} /* namespace Candy */
