#include "Application.h"

namespace Candy {

Application& Application::Instance() {
	static Application application;
	return application;
}

void Application::Initialize() {
	ASSERT(_state == State::Uninitialized);
	if (_state != State::Uninitialized) {
		Log.Error("Initialize failed: application already initialized.");
		return;
	}

	Log.Debug("Initialize.");
	Gfx.Initialize();
	Sfx.Initialize();
	Content.Initialize();
	Content.GetSoundStream().Play(0.3);
	_level.Reset();
	_state = State::Initialized;
}

void Application::Reload() {
	ASSERT(_state != State::Uninitialized);
	if (_state == State::Uninitialized) {
		Log.Error("Reload failed: application not initialized.");
		return;
	}

	Log.Debug("Reload.");
	Gfx.Reload();
	Content.Reload();
}

void Application::Resize(int width, int height) {
	ASSERT(_state != State::Uninitialized);
	ASSERT(width > 0 && height > 0 && width <= height);
	if (_state == State::Uninitialized) {
		Log.Error("Resize failed: application not initialized.");
		return;
	}

	Log.Debug("Resize: width=%d, height=%d.", width, height);
	Gfx.SetViewport(0, 0, width, height);
	_halfWidth = width / 2.f;
	_halfHeight = height / 2.f;
	Gfx.SetOrtho(-_halfWidth, _halfWidth, -_halfHeight, _halfHeight, 1, -1);
	_scale = Min((width / LOGICAL_WIDTH), (height / LOGICAL_HEIGHT));
}

void Application::Resume() {
	ASSERT(_state == State::Initialized || _state == State::Paused);
	if (_state != State::Initialized && _state != State::Paused) {
		Log.Error("Resume application failed: incorrect state=%d.", _state);
		return;
	}

	Log.Debug("Resume.");
	Sfx.Resume();
	_timer.Start();
	_state = State::Resumed;
}

void Application::Loop() {
	ASSERT(_state == State::Resumed);
	if (_state != State::Resumed) {
		Log.Error("Looping failed: application not resumed.");
		return;
	}

	auto deltaTime = _timer.GetDeltaTime();
	ASSERT(deltaTime >= 0);
	constexpr auto MAX_DELTA_TIME = 0.5f;
	if (deltaTime > MAX_DELTA_TIME) {
		Log.Warning("Delta time=%f > max delta time=%f.", deltaTime, MAX_DELTA_TIME);
		deltaTime = MAX_DELTA_TIME;
	}

	Gfx.Begin();
	Gfx.SetMatrixMode(MatrixMode::Projection);
	Gfx.Scale(_scale, _scale);
	Gfx.Translate((LOGICAL_WIDTH / -2.f), (LOGICAL_HEIGHT / -2.f));
	Gfx.ResetMatrixMode();
	_level.Update(deltaTime);
	_level.Draw();
	Gfx.End();

	if (BuildConfig::DEBUG) {
		constexpr float FPS_DURATION = 60;
		static int frames = 0;
		static float seconds = 0;
		++frames;
		seconds += deltaTime;
		if (seconds >= FPS_DURATION) {
			Log.Debug("fps %d", static_cast<int>(frames / seconds));
			frames = 0;
			seconds = 0;
		}
	}
}

void Application::Pause() {
	ASSERT(_state == State::Initialized || _state == State::Resumed);
	if (_state != State::Initialized && _state != State::Resumed) {
		Log.Error("Pause application failed: incorrect state=%d.", _state);
		return;
	}

	Log.Debug("Pause.");
	Sfx.Pause();
	_timer.Stop();
	_state = State::Paused;
} 

void Application::Release() {
	ASSERT(_state != State::Uninitialized);
	if (_state == State::Uninitialized) {
		Log.Error("Release application failed: incorrect state=%d.", _state);
		return;
	}
	if (_state != State::Paused) {
		Log.Warning("Application not paused.");
	}

	Log.Debug("Release.");
	Content.Release();
	Sfx.Release();
	Gfx.Release();
	_state = State::Uninitialized;
}

void Application::TouchDown(float x, float y) {
	ASSERT(_state == State::Resumed);
	if (_state == State::Resumed) {
		TransformTouch(x, y);
		_level.TouchDown(x, y);
	}
}

void Application::TouchMove(float x, float y) {
	ASSERT(_state == State::Resumed);
	if (_state == State::Resumed) {
		TransformTouch(x, y);
		_level.TouchMove(x, y);
	}
}

void Application::TouchUp(float x, float y) {
	ASSERT(_state == State::Resumed);
	if (_state == State::Resumed) {
		TransformTouch(x, y);
		_level.TouchUp(x, y);
	}
}

inline void Application::TransformTouch(float& x, float& y) {
	x = x - _halfWidth;
	y = _halfHeight - y;
	x /= _scale;
	y /= _scale;
	x += LOGICAL_WIDTH / 2.f;
	y += LOGICAL_HEIGHT / 2.f;
}

} /* namespace Candy */
