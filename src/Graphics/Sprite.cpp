#include "Graphics/Sprite.h"

#include "Assert.h"
#include "Content/ContentManager.h"

namespace Core {

Sprite::Sprite(const char* name, float x, float y)
		: _x(x)
		, _y(y)
		, _texture(&Content.GetTexture(name)) {
}

void Sprite::SetTexture(const char* name) {
	_texture = &Content.GetTexture(name);
}

void Sprite::SetPosition(float x, float y) {
	_x = x;
	_y = y;
}

void Sprite::Draw() const {
	_texture->Draw(_x, _y, 0);
}

float Sprite::Left() const {
	return _x;
}

float Sprite::Right() const {
	return _x + Width();
}

float Sprite::Bottom() const {
	return _y;
}

float Sprite::Top() const {
	return _y + Height();
}

int Sprite::Width() const {
	return _texture->Width();
}

int Sprite::Height() const {
	return _texture->Height();
}

} /* namespace Core */
