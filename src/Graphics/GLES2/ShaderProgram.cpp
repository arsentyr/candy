#include "Graphics/GLES2/ShaderProgram.h"

#include <vector>
#include "Assert.h"
#include "Utils/Loggable.h"

namespace Core { namespace {

GLuint CreateShader(GLenum type, const GLchar* source) {
	ASSERT(source != nullptr);
	GLuint shader = glCreateShader(type);
	glShaderSource(shader, 1, &source, nullptr);
	glCompileShader(shader);
	CHECK_GL_ERROR();
	GLint compileStatus = GL_FALSE;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);
	CHECK_GL_ERROR();
	if (!compileStatus) {
		GLint infoLogLength = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
		CHECK_GL_ERROR();
		if (infoLogLength > 0) {
			GLsizei length = 0;
			std::vector<GLchar> infoLog(infoLogLength);
			glGetShaderInfoLog(shader, infoLogLength, &length, infoLog.data());
			CHECK_GL_ERROR();
			infoLog[length] = '\0';
			Log.Error("shader info log: %s", infoLog.data());
		}
	}
	ASSERT(compileStatus);
	return shader;
}

} /* anonymous namespace */

ShaderProgram::~ShaderProgram() {
	ASSERT(!IsProgram());
	if (IsProgram()) {
		Delete();
	}
}

GLuint ShaderProgram::Name() const {
	return _program;
}

void ShaderProgram::Create(const GLchar* sourceVertexShader, const GLchar* sourceFragmentShader) {
	ASSERT(!IsProgram());
	_program = glCreateProgram();
	ASSERT(IsProgram());

	GLboolean hasShaderCompiler = GL_FALSE;
	glGetBooleanv(GL_SHADER_COMPILER, &hasShaderCompiler);
	ASSERT(hasShaderCompiler);

	_vertexShader = CreateShader(GL_VERTEX_SHADER, sourceVertexShader);
	_fragmentShader = CreateShader(GL_FRAGMENT_SHADER, sourceFragmentShader);
	glAttachShader(_program, _vertexShader);
	glAttachShader(_program, _fragmentShader);
	CHECK_GL_ERROR();
}

void ShaderProgram::Link() {
	glLinkProgram(_program);
	CHECK_GL_ERROR();
	GLint linkStatus = GL_FALSE;
	glGetProgramiv(_program, GL_LINK_STATUS, &linkStatus);
	if (!linkStatus) {
		GLint infoLogLength = 0;
		glGetProgramiv(_program, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {
			GLsizei length = 0;
			std::vector<GLchar> infoLog(infoLogLength);
			glGetProgramInfoLog(_program, infoLogLength, &length, infoLog.data());
			CHECK_GL_ERROR();
			infoLog[length] = '\0';
			Log.Error("program info log: %s", infoLog.data());
		}
	}
	ASSERT(linkStatus);

	glDeleteShader(_vertexShader);
	glDeleteShader(_fragmentShader);
	CHECK_GL_ERROR();
	_vertexShader = UNKNOWN_SHADER;
	_fragmentShader = UNKNOWN_SHADER;
}

void ShaderProgram::Delete() {
	glDeleteProgram(_program);
	CHECK_GL_ERROR();
	_program = UNKNOWN_PROGRAM;
	_vertexShader = UNKNOWN_SHADER;
	_fragmentShader = UNKNOWN_SHADER;
}

void ShaderProgram::Lose() {
	ASSERTF(!IsProgram(), "Lost a valid program.");
	_program = UNKNOWN_PROGRAM;
	_vertexShader = UNKNOWN_SHADER;
	_fragmentShader = UNKNOWN_SHADER;
}

bool ShaderProgram::IsProgram() const {
	return _program != UNKNOWN_PROGRAM && glIsProgram(_program);
}

/* Attributes */

void ShaderProgram::BindAttributeLocation(GLuint index, const GLchar* name) {
	ASSERT(name != nullptr);
	glBindAttribLocation(_program, index, name);
	CHECK_GL_ERROR();
}

void ShaderProgram::SetVertexAttributePointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* pointer) {
	glVertexAttribPointer(index, size, type, normalized, stride, pointer);
	CHECK_GL_ERROR();
}

void ShaderProgram::EnableVertexAttributeArray(GLuint index) {
	glEnableVertexAttribArray(index);
	CHECK_GL_ERROR();
}

/* Uniforms */

GLint ShaderProgram::GetUniformLocation(const GLchar* name) const {
	ASSERT(name != nullptr);
	GLint location = glGetUniformLocation(_program, name);
	CHECK_GL_ERROR();
	return location;
}

void ShaderProgram::SetUniform4fv(GLint location, GLsizei count, const GLfloat* fvalues) {
	glUniform4fv(location, count, fvalues);
	CHECK_GL_ERROR();
}

void ShaderProgram::SetUniformMatrix4fv(GLint location, GLsizei count, const GLfloat* fvalues) {
	glUniformMatrix4fv(location, count, GL_FALSE, fvalues);
	CHECK_GL_ERROR();
}

} /* namespace Core */
