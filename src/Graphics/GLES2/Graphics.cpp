#include "Graphics/GLES2/Graphics.h"

#include "Assert.h"
#include "Graphics/Texture.h"
#include "Math/Math.h"

namespace Core { namespace {

constexpr GLchar srcTextureVertexShader[] =
	"attribute vec4 a_position;\n"
	"attribute vec2 a_tex_coord;\n"
	"uniform mat4 u_mvp_matrix;\n"
	"uniform vec4 u_color;\n"   
	"varying mediump vec2 v_tex_coord;\n"
	"varying mediump vec4 v_color;\n"
	"void main() {\n"
	"  v_tex_coord = a_tex_coord;\n"
	"  v_color = u_color;\n"
	"  gl_Position = u_mvp_matrix * a_position;\n"
	"}\n";

constexpr GLchar srcTextureFragmentShader[] =
	"precision lowp float;\n"
	"varying vec2 v_tex_coord;\n"
	"varying vec4 v_color;\n"
	"uniform sampler2D u_texture;\n"
	"void main() {\n"
	"  gl_FragColor = texture2D(u_texture, v_tex_coord) * v_color;\n"
	"}\n";

Graphics graphics;

} /* anonymous namespace */

Graphics& Gfx = graphics;

Graphics::~Graphics() {
	ASSERT(_state == State::Uninitialized);
	if (_state != State::Uninitialized) {
		Release();
	}
}

/* Life cycle */

void Graphics::Initialize() {
	ASSERT(_state == State::Uninitialized);
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	CHECK_GL_ERROR();

	_state = State::Initialized;
	LoadShaderProgram();
	glUseProgram(_shaderProgram.Name());
	CHECK_GL_ERROR();
	ResetBlendMode();
	ResetColor();
}

void Graphics::Reload() {
	ASSERT(_state == State::Initialized);
	_shaderProgram.Lose();
	LoadShaderProgram();
}

void Graphics::Release() {
	ASSERT(_state == State::Initialized);
	UnloadShaderProgram();
	_state = State::Uninitialized;
}

void Graphics::LoadShaderProgram() {
	_shaderProgram.Create(srcTextureVertexShader, srcTextureFragmentShader);
	_shaderProgram.BindAttributeLocation(ATTRIBUTE_POSITION_INDEX, "a_position");
	_shaderProgram.BindAttributeLocation(ATTRIBUTE_TEX_COORD_INDEX, "a_tex_coord");
	_shaderProgram.Link();
	_uniformMvpMatrixLocation = _shaderProgram.GetUniformLocation("u_mvp_matrix");
	_uniformColorLocation = _shaderProgram.GetUniformLocation("u_color");
}

void Graphics::UnloadShaderProgram() {
	_shaderProgram.Delete();
	_uniformMvpMatrixLocation = UNKNOWN_UNIFORM_LOCATION;
	_uniformColorLocation = UNKNOWN_UNIFORM_LOCATION;
}

/* Transforms */

void Graphics::SetViewport(int x, int y, int width, int height) {
	ASSERT(_state != State::Uninitialized);
	ASSERT(width > 0 && height > 0);
	glViewport(x, y, width, height);
	CHECK_GL_ERROR();
}

void Graphics::SetOrtho(float xLeft, float xRight, float yBottom, float yTop, float zNear, float zFar) {
	ASSERT(xLeft < xRight && yBottom < yTop && zNear > zFar);
	ASSERT(_state != State::Uninitialized);
	_projectionMatrix = Matrix4(
		2 / (xRight - xLeft), 0,                    0,                   -(xRight + xLeft) / (xRight - xLeft),
		0,                    2 / (yTop - yBottom), 0,                   -(yTop + yBottom) / (yTop - yBottom),
		0,                    0,                    -2 / (zFar - zNear), -(zFar + zNear) / (zFar - zNear),
		0,                    0,                    0,                   1
	);
}

void Graphics::SetMatrixMode(MatrixMode matrixMode) {
	ASSERT(_state != State::Uninitialized);
	_matrixMode = matrixMode;
}

void Graphics::ResetMatrixMode() {
	SetMatrixMode(MatrixMode::ModelView);
}

void Graphics::PushMatrix() {
	ASSERT(_state == State::Framed);
	switch (_matrixMode) {
	case MatrixMode::ModelView:
		_modelViewStack.PushMatrix();
		break;
	case MatrixMode::Projection:
		_projectionStack.PushMatrix();
		break;
	}
	MvpMatrixChanged();
}

void Graphics::PopMatrix() {
	ASSERT(_state == State::Framed);
	switch (_matrixMode) {
	case MatrixMode::ModelView:
		_modelViewStack.PopMatrix();
		break;
	case MatrixMode::Projection:
		_projectionStack.PopMatrix();
		break;
	}
	MvpMatrixChanged();
}

void Graphics::Scale(float scaleX, float scaleY, float scaleZ) {
	ASSERT(_state == State::Framed);
	switch (_matrixMode) {
	case MatrixMode::ModelView:
		_modelViewStack.Scale(scaleX, scaleY, scaleZ);
		break;
	case MatrixMode::Projection:
		_projectionStack.Scale(scaleX, scaleY, scaleZ);
		break;
	}
	MvpMatrixChanged();
}

void Graphics::Rotate(float angleZ) {
	ASSERT(_state == State::Framed);
	switch (_matrixMode) {
	case MatrixMode::ModelView:
		_modelViewStack.RotateZ(DegToRad(angleZ));
		break;
	case MatrixMode::Projection:
		_projectionStack.RotateZ(DegToRad(angleZ));
		break;
	}
	MvpMatrixChanged();
}

void Graphics::Translate(float deltaX, float deltaY, float deltaZ) {
	ASSERT(_state == State::Framed);
	switch (_matrixMode) {
	case MatrixMode::ModelView:
		_modelViewStack.Translate(deltaX, deltaY, deltaZ);
		break;
	case MatrixMode::Projection:
		_projectionStack.Translate(deltaX, deltaY, deltaZ);
		break;
	}
	MvpMatrixChanged();
}

/* Drawing */

void Graphics::Begin() {
	ASSERT(_state == State::Initialized);
	ASSERT(_modelViewStack.Depth() == 1);
	ASSERT(_projectionStack.Depth() == 1);
	ASSERT(_shaderProgram.IsProgram());
	CHECK_GL_ERROR();

	ResetMatrixMode();
	_modelViewStack.Reset();
	_modelViewStack.LoadIdentity();
	_projectionStack.Reset();
	_projectionStack.LoadMatrix(_projectionMatrix);
	MvpMatrixChanged();
	_state = State::Framed;
}
	
void Graphics::End() {
	ASSERT(_state == State::Framed);
	ASSERT(_modelViewStack.Depth() == 1);
	ASSERT(_projectionStack.Depth() == 1);
	glFlush();
	CHECK_GL_ERROR();
	_state = State::Initialized;
}

void Graphics::SetBlendMode(BlendMode blendMode) {
	ASSERT(_state != State::Uninitialized);
	glBlendEquation(GL_FUNC_ADD);
	switch (blendMode) {
	case BlendMode::Alpha:
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		break;
	case BlendMode::Add:
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		break;
	}
	CHECK_GL_ERROR();
}

void Graphics::ResetBlendMode() {
	SetBlendMode(BlendMode::Alpha);
}

void Graphics::SetColor(float red, float green, float blue, float alpha) {
	ASSERT(_state != State::Uninitialized);
	_color = Color4f::Create(red, green, blue, alpha);
	CHECK_COLOR4F(_color);
	ColorChanged();
}

void Graphics::SetColor(const Color4f& color) {
	ASSERT(_state != State::Uninitialized);
	CHECK_COLOR4F(color);
	_color = color;
	ColorChanged();
}

void Graphics::ResetColor() {
	SetColor(Color4f::White());
}

void Graphics::DrawQuad(const void* vertices, int count) {
	ASSERT(_state == State::Framed && vertices != nullptr && count >= 3);
	if (_isMvpMatrixChanged) {
		_shaderProgram.SetUniformMatrix4fv(_uniformMvpMatrixLocation, 1, (_modelViewStack.TopMatrix() * _projectionStack.TopMatrix()));
		_isMvpMatrixChanged = false;
	}
	constexpr auto stride = sizeof(TextureVertex);
	const auto pointer = static_cast<const float*>(vertices);
	_shaderProgram.SetVertexAttributePointer(ATTRIBUTE_POSITION_INDEX, 3, GL_FLOAT, GL_FALSE, stride, pointer);
	_shaderProgram.SetVertexAttributePointer(ATTRIBUTE_TEX_COORD_INDEX, 2, GL_FLOAT, GL_FALSE, stride, (pointer + 3));
	_shaderProgram.EnableVertexAttributeArray(ATTRIBUTE_POSITION_INDEX);
	_shaderProgram.EnableVertexAttributeArray(ATTRIBUTE_TEX_COORD_INDEX);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, count);
	CHECK_GL_ERROR();
}

inline void Graphics::ColorChanged() {
	_shaderProgram.SetUniform4fv(_uniformColorLocation, 1, &_color.red);
}

inline void Graphics::MvpMatrixChanged() {
	_isMvpMatrixChanged = true;
}

} /* namespace Core */
