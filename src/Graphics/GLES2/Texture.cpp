#include "Graphics/GLES2/Texture.h"

#include "Assert.h"
#include "Graphics/Graphics.h"

namespace Core {

Texture::~Texture() {
	ASSERT(!IsTexture());
	if (IsTexture()) {
		Unload();
	}
} 

int Texture::Width() const {
	return _width;
}

int Texture::Height() const {
	return _height;
}

void Texture::Load(const void* data, int width, int height) {
	ASSERT(data != nullptr);
	ASSERT(width > 0 && (width & (~width + 1)) == width);
	ASSERT(height > 0 && (height & (~height + 1)) == height);
	ASSERT(!IsTexture());
	if (IsTexture()) {
		Unload();
	}
	_width = width;
	_height = height;
	glActiveTexture(GL_TEXTURE0);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	glGenTextures(1, &_texture);
	glBindTexture(GL_TEXTURE_2D, _texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	CHECK_GL_ERROR();
	ASSERT(IsTexture());
}

void Texture::Unload() {
	glDeleteTextures(1, &_texture);
	CHECK_GL_ERROR();
	Lose();
}

void Texture::Lose() {
	ASSERTF(!IsTexture(), "Lost a valid texture.");
	_width = 0;
	_height = 0;
	_texture = UNKNOWN_TEXTURE;
}

bool Texture::IsLoaded() const {
	return _texture != UNKNOWN_TEXTURE;
}

void Texture::Draw(float x, float y,  float z) const {
	Draw(x, y, z, _width, _height, 0, 0, 1, 1);
}

void Texture::Draw(float x, float y, float z, float width, float height, float u0, float v0, float u1, float v1) const {
	ASSERT(width > 0 && height > 0);
	ASSERT(u0 >= 0 && u1 <= 1 && u0 < u1);
	ASSERT(v0 >= 0 && v1 <= 1 && v0 < v1);
	ASSERT(IsLoaded());
	static TextureVertex quad[4];
	quad[0].x = x;         quad[0].y = y;          quad[0].z = z; quad[0].u = u0; quad[0].v = v1;
	quad[1].x = x + width; quad[1].y = y;          quad[1].z = z; quad[1].u = u1; quad[1].v = v1;
	quad[2].x = x;         quad[2].y = y + height; quad[2].z = z; quad[2].u = u0; quad[2].v = v0;
	quad[3].x = x + width; quad[3].y = y + height; quad[3].z = z; quad[3].u = u1; quad[3].v = v0;
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _texture);
	Gfx.DrawQuad(quad, 4);
}

inline bool Texture::IsTexture() const {
	return IsLoaded() && glIsTexture(_texture);
}

} /* namespace Core */
