#include "Graphics/MatrixStack.h"

#include "Assert.h"
#include "Math/Math.h"

namespace Core {

MatrixStack::MatrixStack(unsigned int capacity) {
	_stack.reserve(capacity);
	_stack.push_back(Matrix4::Zero());
}

void MatrixStack::PushMatrix() {
	_stack.push_back(_stack.back());
}

void MatrixStack::PopMatrix() {
	VERIFY(Depth() > 1, "Matrix stack underflow.");
	_stack.pop_back();
}

const Matrix4& MatrixStack::TopMatrix() const {
	return _stack.back();
}

void MatrixStack::LoadIdentity() {
	_stack.back() = Matrix4::Identity();
}

void MatrixStack::LoadMatrix(const Matrix4& mat) {
	_stack.back() = mat;
}

void MatrixStack::LoadMatrix(Matrix4&& mat) {
	_stack.back() = mat;
}

void MatrixStack::MultMatrix(const Matrix4& mat) {
	const auto top = _stack.back();
	_stack.back() = mat * top;
}

void MatrixStack::MultMatrix(Matrix4&& mat) {
	const auto top = _stack.back();
	_stack.back() = mat * top;
}

void MatrixStack::Reset() {
	_stack.clear();
	_stack.push_back(Matrix4::Zero());
}

unsigned int MatrixStack::Depth() const {
	return _stack.size();
}

void MatrixStack::Scale(float scaleX, float scaleY, float scaleZ) {
	MultMatrix(Matrix4(
		scaleX, 0,      0,      0,
		0,      scaleY, 0,      0,
		0,      0,      scaleZ, 0,
		0,      0,      0,      1
	));
}

void MatrixStack::RotateZ(float radians) {
	const auto sine = Sin(radians);
	const auto cosine = Cos(radians);
	MultMatrix(Matrix4(
		cosine, sine,   0, 0,
		-sine,  cosine, 0, 0,
		0,      0,      1, 0,
		0,      0,      0, 1
	));
}

void MatrixStack::Translate(float deltaX, float deltaY, float deltaZ) {
	MultMatrix(Matrix4(
		1,      0,      0,      0,
		0,      1,      0,      0,
		0,      0,      1,      0,
		deltaX, deltaY, deltaZ, 1
	));
}

} /* namespace Core */
