#include "Core.h"

namespace Candy { namespace {

Core::Random rand;

} /* anonymous namespace */

Core::Random& Rand = rand;

} /* namespace Candy */
