#!/bin/sh

export NDK_MODULE_PATH=$(pwd)/third-party
ndk-build NDK_DEBUG=0 #V=1 NDK_LOG=1
if [ $? -eq 0 ]; then
	ant debug install
fi
