#include "Content/AssetStreamReader.h"

#include "Assert.h"

namespace Core {

AAssetManager* AssetStreamReader::_assetManager = nullptr;

AssetStreamReader::~AssetStreamReader() {
	ASSERT(!IsOpen());
	if (IsOpen()) {
		Close();
	}
}

void AssetStreamReader::SetManager(AAssetManager* assetManager) {
	ASSERT(assetManager != nullptr);
	_assetManager = assetManager;
}

/* DataReader Implementation */

const char* AssetStreamReader::Name() const {
	return _name.c_str();
}

bool AssetStreamReader::Open(const char* name) {
	ASSERT(name != nullptr);
	ASSERT(!IsOpen());
	if (IsOpen()) {
		Close();
	}
	ASSERT(_assetManager != nullptr);
	_asset = AAssetManager_open(_assetManager, name, AASSET_MODE_UNKNOWN);
	if (IsOpen()) {
		_name = name;
		_offset = 0;
	}
	ASSERT(IsOpen());
	return IsOpen();
}

bool AssetStreamReader::IsOpen() const {
	return _asset != nullptr;
}

long int AssetStreamReader::Read(void* buffer, unsigned int count) {
	ASSERT(buffer != nullptr && count > 0);
	ASSERT(IsOpen());
	const auto result = AAsset_read(_asset, buffer, count);
	ASSERT(result >= 0);
	if (result > 0) {
		_offset += result;
	}
	return result;
}

long int AssetStreamReader::Seek(long int offset, int whence) {
	ASSERT(IsOpen());
	const auto result = AAsset_seek(_asset, offset, whence);
	ASSERT(result >= 0);
	if (result >= 0) {
		_offset = result;
	}
	return result;
}

long int AssetStreamReader::Tell() {
	ASSERT(IsOpen());
	return _offset;
}

long int AssetStreamReader::Length() const {
	ASSERT(IsOpen());
	return AAsset_getLength(_asset);
}

void AssetStreamReader::Close() {
	ASSERT(IsOpen());
	AAsset_close(_asset);
	_asset = nullptr;
	_offset = 0;
	_name.clear();
}

} /* namespace Core */
