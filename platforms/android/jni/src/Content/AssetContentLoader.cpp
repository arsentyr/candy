#include "Content/AssetContentLoader.h"

#include "Assert.h"
#include "Content/Image.h"

namespace Core { namespace {

AssetContentLoader loader;

} /* anonymous namespace */

ContentLoader& Loader = loader;

void AssetContentLoader::LoadTexture(Texture& texture, const std::string& path) {
	unsigned int width = 0;
	unsigned int height = 0;
	_reader.Open(path.c_str());
	VERIFY(_reader.IsOpen(), "Texture %s not openned.", _reader.Name());
	auto buffer = Buffer();
	DecodePng(_reader, buffer, &width, &height);
	_reader.Close();
	texture.Load(buffer, static_cast<int>(width), static_cast<int>(height));
}

void AssetContentLoader::LoadSoundSample(SoundSample& sample, const std::string& path) {
	_reader.Open(path.c_str());
	VERIFY(_reader.IsOpen(), "Sound %s not openned.", _reader.Name());
	auto buffer = Buffer();
	const auto length = _reader.Length();
	ASSERT(BuildConfig::LOADER_BUFFER_SIZE >= length);
	_reader.Read(buffer, length);
	_reader.Close();
	sample.Load(buffer, length);
}

void AssetContentLoader::LoadSoundStream(SoundStream& stream, const std::string& path) {
	_reader.Open(path.c_str());
	VERIFY(_reader.IsOpen(), "Sound %s not openned.", _reader.Name());
	stream.Load(_reader);
	_reader.Close();
}

} /* namespace Core */
