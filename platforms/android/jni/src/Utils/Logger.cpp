#include "Utils/Logger.h"

#include <android/log.h>
#include "BuildConfig.h"

namespace Core { namespace {

Logger log;

} /* anonymous namespace */

Loggable& Log = log;

void Logger::Debug(const char* format, ...) {
	if (BuildConfig::LOGGABLE_DEBUG) {
		va_list args;
		va_start(args, format);
		__android_log_vprint(ANDROID_LOG_DEBUG, LOG_TAG, format, args);
		va_end(args);
	}
}

void Logger::Warning(const char* format, ...) {
	if (BuildConfig::LOGGABLE_WARNING) {
		va_list args;
		va_start(args, format);
		__android_log_vprint(ANDROID_LOG_WARN, LOG_TAG, format, args);
		va_end(args);
	}
}

void Logger::Error(const char* format, ...) {
	if (BuildConfig::LOGGABLE_ERROR) {
		va_list args;
		va_start(args, format);
		__android_log_vprint(ANDROID_LOG_ERROR, LOG_TAG, format, args);
		va_end(args);
	}
}

} /* namespace Core */
