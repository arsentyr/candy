#ifndef CORE_ASSET_CONTENT_LOADER_H
#define CORE_ASSET_CONTENT_LOADER_H

#include "Content/AssetStreamReader.h"
#include "Content/ContentLoader.h"

namespace Core {

/* AssetContentLoader */

class AssetContentLoader : public ContentLoader {
public:
	AssetContentLoader() = default;
	AssetContentLoader(const AssetContentLoader&) = delete;
	AssetContentLoader(AssetContentLoader&&) = delete;

	AssetContentLoader& operator=(const AssetContentLoader&) = delete;
	AssetContentLoader& operator=(AssetContentLoader&&) = delete;

	virtual ~AssetContentLoader() = default;

public: /* ContentLoader Implementation */
	void LoadTexture(Texture& texture, const std::string& path) override;

	void LoadSoundSample(SoundSample& sample, const std::string& path) override;

	void LoadSoundStream(SoundStream& stream, const std::string& path) override;

private:
	AssetStreamReader _reader;
};

} /* namespace Core */

#endif /* CORE_ASSET_CONTENT_LOADER_H */
