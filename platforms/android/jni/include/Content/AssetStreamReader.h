#ifndef CORE_ASSET_STREAM_READER_H
#define CORE_ASSET_STREAM_READER_H

#include <string>
#include <android/asset_manager.h>
#include "Content/DataReader.h"

namespace Core {

/* AssetStreamReader */

class AssetStreamReader : public DataReader {
public:
	AssetStreamReader() = default;
	AssetStreamReader(const AssetStreamReader&) = delete;
	AssetStreamReader(AssetStreamReader&&) = delete;

	AssetStreamReader& operator=(const AssetStreamReader&) = delete;
	AssetStreamReader& operator=(AssetStreamReader&&) = delete;

	virtual ~AssetStreamReader();

	static void SetManager(AAssetManager* assetManager);

public: /* DataReader Implementation */
	const char* Name() const override;

	bool Open(const char* name) override;

	bool IsOpen() const override;

	long int Read(void* buffer, unsigned int count) override;

	long int Seek(long int offset, int whence) override;

	long int Tell() override;

	long int Length() const override;

	void Close() override;

private:
	static AAssetManager* _assetManager;

	AAsset* _asset = nullptr;
	long int _offset = 0;
	std::string _name;
};

} /* namespace Core */

#endif /* CORE_ASSET_STREAM_READER_H */
