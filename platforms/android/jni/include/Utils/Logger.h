#ifndef CORE_LOGGER_H
#define CORE_LOGGER_H

#include "Utils/Loggable.h"

namespace Core {

/* Logger */

class Logger : public Loggable {
public:
	Logger() = default;
	Logger(const Logger&) = delete;
	Logger(Logger&&) = delete;

	Logger& operator=(const Logger&) = delete;
	Logger& operator=(Logger&&) = delete;

	virtual ~Logger() = default;

public: /* Loggable Implementation */
	void Debug(const char* format, ...) override;

	void Warning(const char* format, ...) override;

	void Error(const char* format, ...) override;
};

} /* namespace Core */

#endif /* CORE_LOGGER_H */
