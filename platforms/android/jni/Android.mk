LOCAL_PATH := $(call my-dir)

MY_LOG_TAG := \"Candy\"

include $(CLEAR_VARS)

LOCAL_MODULE := candy

MY_SRC := ../../../src
LOCAL_SRC_FILES += \
	$(MY_SRC)/Application.cpp \
	$(MY_SRC)/Board.cpp \
	$(MY_SRC)/Button.cpp \
	$(MY_SRC)/Cell.cpp \
	$(MY_SRC)/Chip.cpp \
	$(MY_SRC)/Core.cpp \
	$(MY_SRC)/Effect.cpp \
	$(MY_SRC)/Level.cpp \
	$(MY_SRC)/OmNom.cpp \
	$(MY_SRC)/Progress.cpp

MY_SRC_ANIMATION = $(MY_SRC)/Animation
LOCAL_SRC_FILES += \
	$(MY_SRC_ANIMATION)/Animation.cpp \
	$(MY_SRC_ANIMATION)/TweenEquation.cpp

MY_SRC_AUDIO = $(MY_SRC)/Audio
LOCAL_SRC_FILES += \
	$(MY_SRC_AUDIO)/Sound.cpp \
	$(MY_SRC_AUDIO)/SoundSample.cpp \
	$(MY_SRC_AUDIO)/SoundStream.cpp

MY_SRC_CONTENT = $(MY_SRC)/Content
LOCAL_SRC_FILES += \
	$(MY_SRC_CONTENT)/ContentManager.cpp \
	$(MY_SRC_CONTENT)/Image.cpp

MY_SRC_GRAPHICS := $(MY_SRC)/Graphics
LOCAL_SRC_FILES += \
	$(MY_SRC_GRAPHICS)/MatrixStack.cpp \
	$(MY_SRC_GRAPHICS)/Sprite.cpp \
	$(MY_SRC_GRAPHICS)/GLES2/Graphics.cpp \
	$(MY_SRC_GRAPHICS)/GLES2/ShaderProgram.cpp \
	$(MY_SRC_GRAPHICS)/GLES2/Texture.cpp

MY_SRC_MATH := $(MY_SRC)/Math
LOCAL_SRC_FILES += \
	$(MY_SRC_MATH)/Matrix4.cpp

MY_SRC_UTILS := $(MY_SRC)/Utils
LOCAL_SRC_FILES += \
	$(MY_SRC_UTILS)/Random.cpp \
	$(MY_SRC_UTILS)/Timer.cpp

LOCAL_SRC_FILES += \
	src/candy.cpp \
	src/Content/AssetStreamReader.cpp \
	src/Content/AssetContentLoader.cpp \
	src/Utils/Logger.cpp
 
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/include

LOCAL_CFLAGS += -DLOG_TAG=$(MY_LOG_TAG)
LOCAL_CFLAGS += -D__cplusplus=201103L # for ADT plugin

LOCAL_STATIC_LIBRARIES := libpng
LOCAL_SHARED_LIBRARIES := bass
LOCAL_LDLIBS += -llog -landroid -lGLESv2 -lz

include $(BUILD_SHARED_LIBRARY)

$(call import-module, libpng-1.6.2)
$(call import-module, bass-2.4.10)
