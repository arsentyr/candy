#include "Content/FileReader.h"

#include "Assert.h"

namespace Core {

FileReader::~FileReader() {
	ASSERT(!IsOpen());
	if (IsOpen()) {
		Close();
	}
}

const char* FileReader::Name() const {
	return _name.c_str();
}

bool FileReader::Open(const char* name) {
	ASSERT(name != nullptr);
	ASSERT(!IsOpen());
	if (IsOpen()) {
		Close();
	}
	_file = fopen(name, "r");
	ASSERT(_file != nullptr);
	if (IsOpen()) {
		_name = name;
		return true;
	}
	return false;
}

bool FileReader::IsOpen() const {
	return _file != nullptr;
}

long int FileReader::Read(void* buffer, unsigned int count) {
	ASSERT(IsOpen());
	return fread(buffer, 1, count, _file);
}

long int FileReader::Seek(long int offset, int whence) {
	ASSERT(IsOpen());
	return fseek(_file, offset, whence);
}

long int FileReader::Tell() {
	ASSERT(IsOpen());
	return ftell(_file);
}

long int FileReader::Length() const {
	if (IsOpen()) {
		auto tell = ftell(_file);
		fseek(_file, 0, SEEK_END);
		auto length = ftell(_file);
		ASSERT(length > 0);
		fseek(_file, tell, SEEK_SET);
		if (length > 0) {
			return length;
		}
	}
	return 0;
}

void FileReader::Close() {
	ASSERT(IsOpen());
	fclose(_file);
	_file = nullptr;
	_name.clear();
}

} /* namespace Core */
