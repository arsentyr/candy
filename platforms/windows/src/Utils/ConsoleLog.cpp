#include "Utils/ConsoleLog.h"

#include <cstdarg>
#include <cstdio>
#include "BuildConfig.h"

namespace Core { namespace {

ConsoleLog log;

} /* anonymous namespace */

Loggable& Log = log;

void ConsoleLog::Debug(const char* format, ...) {
	if (BuildConfig::LOGGABLE_DEBUG) {
		va_list args;
		va_start(args, format);
		vprintf(format, args);
		va_end(args);
		putchar('\n');		
	}
}

void ConsoleLog::Warning(const char* format, ...) {
	if (BuildConfig::LOGGABLE_WARNING) {
		va_list args;
		va_start(args, format);
		vprintf(format, args);
		va_end(args);
		putchar('\n');
	}
}

void ConsoleLog::Error(const char* format, ...) {
	if (BuildConfig::LOGGABLE_ERROR) {
		va_list args;
		va_start(args, format);
		vprintf(format, args);
		va_end(args);
		putchar('\n');
	}
}

} /* namespace Core */
