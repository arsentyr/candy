#include <windows.h>
#include <EGL/egl.h>
#include "Assert.h"
#include "Application.h"

#define CHECK_EGL_ERROR() do { auto err = eglGetError(); ASSERTF(err == EGL_SUCCESS, "%d", err); } while (false)

namespace {

bool isLooping = true;
bool hasTouch = false;

} /* anonymous namespace */

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    auto& app = Candy::Application::Instance();
    switch(uMsg) {
    case WM_LBUTTONDOWN:
        if (hasTouch) {
            app.TouchUp(LOWORD(lParam), HIWORD(lParam));
        }
        hasTouch = true;
        app.TouchDown(LOWORD(lParam), HIWORD(lParam));
        break;

    case WM_LBUTTONUP:
        if (hasTouch) {
            app.TouchUp(LOWORD(lParam), HIWORD(lParam));
            hasTouch = false;
        }
        break;

    case WM_MOUSEMOVE:
        if (hasTouch) {
            app.TouchMove(LOWORD(lParam), HIWORD(lParam));
        }
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        isLooping = false;
        break;

    default:
        return DefWindowProc(hWnd, uMsg, wParam, lParam);
    }
    return 0;
}

int WINAPI WinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/, TCHAR* /*lpCmdLine*/, int /*nCmdShow*/) {
    HINSTANCE hInstance = GetModuleHandle(nullptr);
    TCHAR szWindowName[50] = TEXT("Candy");
    TCHAR szClassName[50] = TEXT("CANDY_CLASS");

    WNDCLASS wndClass;
    wndClass.lpszClassName = szClassName;
    wndClass.lpfnWndProc = (WNDPROC) WndProc;
    wndClass.hInstance = hInstance;
    wndClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wndClass.hIcon = LoadIcon(nullptr, IDI_WINLOGO);
    wndClass.hbrBackground = nullptr;
    wndClass.lpszMenuName = nullptr;
    wndClass.style = CS_HREDRAW | CS_OWNDC | CS_VREDRAW;
    wndClass.cbClsExtra = 0;
    wndClass.cbWndExtra = 0;
    VERIFY(RegisterClass(&wndClass), "Windows error: %lu.", GetLastError());

    constexpr int width = 480;
    constexpr int height = 800;
    RECT windowRect;
    windowRect.left = 0;
    windowRect.right = width;
    windowRect.top = 0;
    windowRect.bottom = height;
    DWORD dwExtStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
    DWORD dwWindStyle = WS_OVERLAPPEDWINDOW;
    AdjustWindowRectEx(&windowRect, dwWindStyle, FALSE, dwExtStyle);
    auto hWnd = CreateWindowEx(
        dwExtStyle,
        szClassName,
        szWindowName,
        dwWindStyle | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
        0,
        0,
        (windowRect.right - windowRect.left),
        (windowRect.bottom - windowRect.top),
        nullptr,
        nullptr,
        hInstance,
        nullptr);
    ASSERT(hWnd != nullptr);
	auto hDC = GetDC(hWnd);
    ASSERT(hDC != nullptr);

    ShowWindow(hWnd, SW_SHOWDEFAULT);
    ShowCursor(TRUE);

    eglBindAPI(EGL_OPENGL_ES_API);
    CHECK_EGL_ERROR();
	EGLDisplay display = eglGetDisplay(hDC);
	ASSERT(display != 0);
	eglInitialize(display, nullptr, nullptr);
	CHECK_EGL_ERROR();
	EGLint surfaceAttribs[] = {
		EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_NONE 
	};
	EGLint numConfigs = 0;
	EGLConfig surfaceConfig = 0;
	eglChooseConfig(display, surfaceAttribs, &surfaceConfig, 1, &numConfigs);
	ASSERT(numConfigs == 1 && surfaceConfig != 0);
	CHECK_EGL_ERROR();
	EGLSurface surface = eglCreateWindowSurface(display, surfaceConfig, hWnd, nullptr);
	ASSERT(surface != 0);
	EGLint contextAttribs[] = {
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};
	EGLContext context = eglCreateContext(display, surfaceConfig, EGL_NO_CONTEXT, contextAttribs);
	ASSERT(context != 0);
	eglMakeCurrent(display, surface, surface, context);
	CHECK_EGL_ERROR();

    auto& app = Candy::Application::Instance();
	app.Initialize();
    app.Resize(width, height);
    app.Resume();
	MSG msg;
    while (isLooping) {
        while (PeekMessage(&msg, hWnd, 0, 0, PM_REMOVE) != 0) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        app.Loop();
        eglSwapBuffers(display, surface);   
        Sleep(30);
    }
	app.Release();

	eglDestroyContext(display, context);
	eglDestroySurface(display, surface);
	eglTerminate(display);
	CHECK_EGL_ERROR();

  	ReleaseDC(hWnd, hDC);
  	DestroyWindow(hWnd);
  	UnregisterClass(szClassName, hInstance);
	return 0;
}
