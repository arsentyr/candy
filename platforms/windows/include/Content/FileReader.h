#ifndef CORE_FILE_READER_H
#define CORE_FILE_READER_H

#include <cstdio>
#include <string>
#include "Content/DataReader.h"

namespace Core {

/* FileReader */

class FileReader : public DataReader {
public:
	FileReader() = default;

	FileReader(const FileReader&) = delete;
	FileReader(FileReader&&) = delete;

	FileReader& operator=(const FileReader&) = delete;
	FileReader& operator=(FileReader&&) = delete;

	virtual ~FileReader();

public: /* IDataReader Implementation */
	const char* Name() const override;

	bool Open(const char* name) override;

	bool IsOpen() const override;

	long int Read(void* buffer, unsigned int count) override;

	long int Seek(long int offset, int whence) override;

	long int Tell() override;

	long int Length() const override;

	void Close() override;

private:
	FILE* _file = nullptr;
	std::string _name;
};

} /* namespace Core */

#endif /* CORE_FILE_READER_H */
