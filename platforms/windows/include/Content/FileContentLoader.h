#ifndef CORE_FILE_CONTENT_LOADER_H
#define CORE_FILE_CONTENT_LOADER_H

#include "Content/ContentLoader.h"
#include "Content/FileReader.h"

namespace Core {

/* FileContentLoader */

class FileContentLoader : public ContentLoader {
public:
	FileContentLoader() = default;

	FileContentLoader(const FileContentLoader&) = delete;
	FileContentLoader(FileContentLoader&&) = delete;

	FileContentLoader& operator=(const FileContentLoader&) = delete;
	FileContentLoader& operator=(FileContentLoader&&) = delete;

	virtual ~FileContentLoader() = default;

public:
	void LoadTexture(Texture& texture, const std::string& path) override;

	void LoadSoundSample(SoundSample& sample, const std::string& path) override;

	void LoadSoundStream(SoundStream& stream, const std::string& path) override;

private:
	FileReader _reader;
};

} /* namespace Core */

#endif /* CORE_FILE_CONTENT_LOADER_H */
