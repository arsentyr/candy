#ifndef CORE_CONSOLE_LOG_H
#define CORE_CONSOLE_LOG_H

#include "Utils/Loggable.h"

namespace Core {

/* ConsoleLog */

class ConsoleLog : public Loggable {
public:
	ConsoleLog() = default;
	ConsoleLog(const ConsoleLog&) = delete;
	ConsoleLog(ConsoleLog&&) = delete;

	ConsoleLog& operator=(const ConsoleLog&) = delete;
	ConsoleLog& operator=(ConsoleLog&&) = delete;

	virtual ~ConsoleLog() = default;

public: /* Loggable Implementation */
	void Debug(const char* format, ...) override;

	void Warning(const char* format, ...) override;

	void Error(const char* format, ...) override;
};

} /* namespace Core */

#endif /* CORE_CONSOLE_LOG_H */
